[[_TOC_]]

## Overview

**Windows Feature Update UI** is intended to serve as a user-facing frontend for executing a **ConfigMgr Windows 10 Upgrade Task Sequence**. Prior to running the **Upgrade Task Sequence**, the application performs various pre-flight checks which minimize the possibility of a failed upgrade.

## Download
[Windows Feature Update UI 1.0.7.1](https://gitlab.com/max.bado/windows-feature-update-ui/uploads/fc8002431e44604ee0d1bd912d0aa78c/W10UpgUI_1.0.7.1.zip)

## Features

*  Customizable verbiage, theme, and logo
*  Performs the following pre-flight checks:
   *  Windows Build Version
   *  Space
   *  Memory
   *  Power Status
   *  Network Status
   *  External Storage Status
*  Support for multiple **Task Sequences** based on OS SKU
*  Verbose logging with options to copy error logs to an external share
*  Locking down of machine while OS upgrades
*  Support to execute custom scripts
*  Customizable Failure / Success messages

## In Action

*  Allow the user to upgrade their **Windows 10** using **Software Center** 

![2019-03-28_14_13_11-Software_Center](https://gitlab.com/max.bado/windows-feature-update-ui/-/wikis/uploads/e6e53fcbd21b9d5e6ff156b95a31c0fe/2019-03-28_14_13_11-Software_Center.png)

*  Clicking **Install** would execute the **Windows Feature Update UI** to guide the user through starting the upgrade

![2020-05-16_09_34_11-Windows_Feature_Update](https://gitlab.com/max.bado/windows-feature-update-ui/-/wikis/uploads/bd7232e28be9df799cafa2091835acec/2020-05-16_09_34_11-Windows_Feature_Update.png)

*  The application performs pre-flight checks

![2019-03-28_14_19_15-Greenshot](https://gitlab.com/max.bado/windows-feature-update-ui/-/wikis/uploads/4a1e0d9c74549cadca7ff53dcb05edf1/2019-03-28_14_19_15-Greenshot.png)

*  Once the checks pass, the user can start the upgrade

![2020-05-16_12_23_09-Windows_Feature_Update](https://gitlab.com/max.bado/windows-feature-update-ui/-/wikis/uploads/872bd8f0d206bb32a41e9cad40e4ed67/2020-05-16_12_23_09-Windows_Feature_Update.png)

*  A full screen notice is displayed during the upgrade, preventing the user from using the device

![2020-05-16_13_40_10-UPGBackground](https://gitlab.com/max.bado/windows-feature-update-ui/-/wikis/uploads/2b61edd4a64471ca823330f46f8f14c7/2020-05-16_13_40_10-UPGBackground.png)

