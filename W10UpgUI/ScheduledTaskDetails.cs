﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace W10UpgUI
{
    class ScheduledTaskDetails
    {
        public string ScheduledTaskName { get; set; }
        public List<ScheduledTaskAction> ScheduledTaskActions { get; set; }
        public bool ScheduledTaskModifiedFlag { get; set; }
        public static NLog.Logger logger;

        public ScheduledTaskDetails(string scheduledTaskXmlPath)
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            CreateScheduledTaskDetails(scheduledTaskXmlPath);
        }

        private void CreateScheduledTaskDetails(string scheduledTaskXmlPath)
        {
            logger.Info($"BEGIN Creating Scheduled Task Details object.");
            //ScheduledTaskDetails scheduledTaskDetails = null;
            bool scheduledTaskModifiedFlag = false;

            ScheduledTaskActions = new List<ScheduledTaskAction>();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(scheduledTaskXmlPath);
            XmlNode root = xmlDocument.DocumentElement;
            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmlDocument.NameTable);
            xmlnsManager.AddNamespace("def", "http://schemas.microsoft.com/windows/2004/02/mit/task");

            // Get Scheduled Task Name
            XmlNode xmlParentNode = root.SelectSingleNode(@"/def:Task/def:RegistrationInfo/def:URI", xmlnsManager);
            
            if(xmlParentNode != null)
            {
                this.ScheduledTaskName = xmlParentNode.InnerText.Trim('\\');
            }

            // Get Scheduled Task Actions
            xmlParentNode = root.SelectSingleNode(@"/def:Task/def:Actions", xmlnsManager);

            if (xmlParentNode != null)
            {
                //scheduledTaskDetails = new ScheduledTaskDetails();
                foreach (XmlNode xmlNode in xmlParentNode.ChildNodes)
                {
                    ScheduledTaskAction scheduledTaskAction = new ScheduledTaskAction();
                    XmlNode commandNode = xmlNode["Command"];
                    XmlNode argumentsNode = xmlNode["Arguments"];
                    XmlNode workingDirectoryNode = xmlNode["WorkingDirectory"];

                    string scheduledTaskCommandPath = null;
                    string scheduledTaskCommandValidatedPath = null;
                    string scheduledTaskArguments = null;
                    string scheduledTaskWorkingDirectoryPath = null;

                    if (workingDirectoryNode != null)
                    {
                        scheduledTaskWorkingDirectoryPath = workingDirectoryNode.InnerText.Trim();

                        // Check if the directory exists
                        logger.Info($"Checking if '{scheduledTaskWorkingDirectoryPath}' exists in the current context.");

                        // Expand variables
                        logger.Info($"Attempting to expand any environment variables...");
                        scheduledTaskWorkingDirectoryPath = Environment.ExpandEnvironmentVariables(scheduledTaskWorkingDirectoryPath);
                        logger.Info($"Path post variable expansion: '{scheduledTaskWorkingDirectoryPath}'");

                        DirectoryInfo workingDirectoryDirectoryInfo = new DirectoryInfo(scheduledTaskWorkingDirectoryPath);

                        if (workingDirectoryDirectoryInfo.Exists) // Directory exists
                        {
                            logger.Info($"'{scheduledTaskWorkingDirectoryPath}' exists. Using the full path, '{workingDirectoryDirectoryInfo.FullName}'");
                            scheduledTaskWorkingDirectoryPath = workingDirectoryDirectoryInfo.FullName;
                        }
                        else // Can't find directory
                        {
                            logger.Error($"Unable to find Scheduled Task working directory path: {scheduledTaskWorkingDirectoryPath}");
                        }
                    } else
                    {
                        logger.Warn($"No Working Directory specified");
                    }

                    if (commandNode != null)
                    {
                        scheduledTaskCommandPath = commandNode.InnerText.Trim();

                        // Check if the file exists
                        logger.Info($"Checking if '{scheduledTaskCommandPath}' exists in the current context.");

                        // Expand variables
                        logger.Info($"Attempting to expand any environment variables...");
                        scheduledTaskCommandPath = Environment.ExpandEnvironmentVariables(scheduledTaskCommandPath);
                        logger.Info($"Path post variable expansion: '{scheduledTaskCommandPath}'");

                        FileInfo commandFileInfo = new FileInfo(scheduledTaskCommandPath);

                        if (commandFileInfo.Exists) // File exists
                        {
                            logger.Info($"'{scheduledTaskCommandPath}' exists. Using the full path, '{commandFileInfo.FullName}'");
                            scheduledTaskCommandValidatedPath = commandFileInfo.FullName;
                        }
                        else // File does not exist... Check if Windows knows where the command file is located 
                        {
                            logger.Warn($"'{scheduledTaskCommandPath}' does not exist in the current context...");
                            logger.Info($"Checking if '{scheduledTaskCommandPath}' can be resolved to a full path by the OS.");
                            scheduledTaskCommandValidatedPath = App.GetFullPathFromWindows(scheduledTaskCommandPath);

                            if (scheduledTaskCommandValidatedPath != null) // Windows knows where this file is
                            {
                                logger.Info($"Windows resolved file path for '{scheduledTaskCommandPath}' to the full path '{scheduledTaskCommandValidatedPath}'");

                            }
                            else // Windows does not know where this file is...
                            {
                                logger.Warn($"Windows was NOT able to resolve the file path for '{scheduledTaskCommandPath}'");

                                // Check if a working directory is provided and then test to see if the command file lives in there
                                if (scheduledTaskWorkingDirectoryPath != null) // Working directory was provided
                                {
                                    logger.Info($"The following working directory is provided: '{scheduledTaskWorkingDirectoryPath}'");

                                    // full path = working directory + command file path
                                    scheduledTaskCommandValidatedPath = System.IO.Path.Combine(scheduledTaskWorkingDirectoryPath, scheduledTaskCommandPath);
                                    logger.Info($"Checking if the command file exists in the following location: '{scheduledTaskCommandValidatedPath}'");

                                    if (File.Exists(scheduledTaskCommandValidatedPath)) // Command path is valid
                                    {

                                        logger.Info($"The command file was found at the following location: '{scheduledTaskCommandValidatedPath}'");
                                    }
                                    else // Command path is not valid. Throw error
                                    {
                                        logger.Warn($"The command file was NOT found at the following location: '{scheduledTaskCommandValidatedPath}'");
                                        scheduledTaskCommandValidatedPath = null;
                                    }
                                }
                                else // No working directory, so we need to check if the command path can be resolved by its absolute path to the current directory
                                {
                                    scheduledTaskCommandValidatedPath = App.GetAbsolutePath(scheduledTaskCommandPath);
                                    logger.Warn($"A working directory was not provided for the file '{scheduledTaskCommandPath}'. Checking if the file exists at '{scheduledTaskCommandValidatedPath}'");

                                    if (File.Exists(scheduledTaskCommandValidatedPath))  // Command file exists at the absolute path
                                    {
                                        logger.Info($"The command file was found at the absolute path: '{scheduledTaskCommandValidatedPath}'");
                                    }
                                    else // Command file does not exist at the absolute path
                                    {
                                        logger.Error($"The command file was NOT found at the absolute path: '{scheduledTaskCommandValidatedPath}'. Unable to continue");
                                        scheduledTaskCommandValidatedPath = null;
                                    }
                                }
                            }
                        }

                        if (scheduledTaskCommandPath != scheduledTaskCommandValidatedPath)
                        {
                            scheduledTaskModifiedFlag = true;
                        }
                    } else
                    {
                        logger.Warn($"No Command specified");
                    }

                    if (argumentsNode != null)
                    {
                        scheduledTaskArguments = argumentsNode.InnerText.Trim();
                        logger.Info($"Scheduled Task command arguments: {scheduledTaskArguments}");
                    } else
                    {
                        logger.Warn($"No Arguments specified");
                    }


                    scheduledTaskAction.ActionCommand = scheduledTaskCommandValidatedPath;
                    scheduledTaskAction.ActionArguments = scheduledTaskArguments;
                    scheduledTaskAction.ActionWorkingDirectory = scheduledTaskWorkingDirectoryPath;

                    // Add the scheduledTaskAction object to the list
                    this.ScheduledTaskActions.Add(scheduledTaskAction);
                }

                //scheduledTaskDetails.ScheduledTaskActions = scheduledTaskActions;
                this.ScheduledTaskModifiedFlag = scheduledTaskModifiedFlag;
            }

            //return scheduledTaskDetails;
        }
    }
}
