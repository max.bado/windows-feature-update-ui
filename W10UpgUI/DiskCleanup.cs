﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class DiskCleanup
    {
        public static NLog.Logger logger;

        public DiskCleanup()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        // START RemoveUserTempData method
        public void RemoveUserTempData()
        {
            string usersDirPath = Environment.GetEnvironmentVariable("SystemDrive") + @"\Users";
            var userDirList = new DirectoryInfo(usersDirPath);
            IEnumerable<DirectoryInfo> userDirQuery = from dir in userDirList.EnumerateDirectories()
                                                      where (!(dir.Attributes.ToString().Contains("Hidden")))
                                                      where (!(dir.Attributes.ToString().Contains("System")))
                                                      where (!(dir.Attributes.ToString().Contains("ReparsePoint")))
                                                      select dir;


            foreach (var userDir in userDirQuery)
            {
                string tempDirPath = userDir.FullName + @"\AppData\Local\Temp";

                if (Directory.Exists(tempDirPath))
                {
                    var tempDir = new DirectoryInfo(tempDirPath);
                    foreach (var file in tempDir.EnumerateFiles())
                    {
                        try
                        {
                            file.Delete();
                        }
                        catch (Exception e)
                        {
                            logger.Warn($"Unable to delete {file.FullName}: {e}");
                        }
                    }

                    foreach (var dir in tempDir.EnumerateDirectories())
                    {
                        try
                        {
                            dir.Delete(true);
                        }
                        catch (Exception e)
                        {
                            logger.Warn($"Unable to delete {dir.FullName}: {e}");
                        }
                    }
                }
                else
                {
                    logger.Warn($"The path '{tempDirPath}' does not exist");
                }
            }
        }
        // END RemoveUserTempData

        // START RemoveWindowsTempData
        public void RemoveWindowsTempData()
        {
            string windowsTempPath = Environment.GetEnvironmentVariable("windir") + @"\Temp";
            var windowsTempDir = new DirectoryInfo(windowsTempPath);

            foreach (var file in windowsTempDir.EnumerateFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception e)
                {
                    logger.Warn($"Unable to delete {file.FullName}: {e}");
                }
            }

            foreach (var dir in windowsTempDir.EnumerateDirectories())
            {
                try
                {
                    dir.Delete(true);
                }
                catch (Exception e)
                {
                    logger.Warn($"Unable to delete {dir.FullName}: {e}");
                }
            }
        }
        // END RemoveWindowsTempData
    }
}
