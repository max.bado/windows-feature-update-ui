﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Windows.Markup;
using System.Windows.Threading;
//using NLog.Config;
//using NLog;

namespace W10UpgUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;

        public static MainWindow mainWindow;
        public static ComputerInfo computerInfo; 
        public static Settings settings;
        public static NLog.Logger logger;
        public bool uiSession = false;
        public static string currentApplicationDirectory;
        private string logPath = $"{Environment.GetEnvironmentVariable("windir")}\\Logs\\Software\\WindowsFeatureUpdateUI.log"; // Set a default log path
        private string settingsFileName = "settings.json";
        public static string taskSequenceName;


        [STAThread]
        protected override void OnStartup(StartupEventArgs e)
        {
            // add custom accent and theme resource dictionaries to the ThemeManager
            // you should replace MahAppsMetroThemesSample with your application name
            // and correct place where your custom accent lives
            //ThemeManager.AddAccent("CustomAccent1", new Uri("pack://application:,,,/W10UpgUI;component/resources/CustomAccent1.xaml"));

            // get the current app style (theme and accent) from the application
            //Tuple<AppTheme, Accent> theme = ThemeManager.DetectAppStyle(Application.Current);

            // now change app style to the custom accent and current theme
            //ThemeManager.ChangeAppStyle(Application.Current,
            //                            ThemeManager.GetAccent("CustomAccent1"),
            //                            theme.Item1);



            base.OnStartup(e);

            // Configure logging. Reference: https://github.com/NLog/NLog/wiki/Tutorial
            var config = new NLog.Config.LoggingConfiguration();
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = logPath };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);
            NLog.LogManager.Configuration = config;
            logger = NLog.LogManager.GetCurrentClassLogger();

            // Get the current application directory
            currentApplicationDirectory = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);

            // Change directory to the application directory if different from current working directory
            if (Environment.CurrentDirectory != currentApplicationDirectory)
            {
                logger.Warn($"Setting current directory to '{currentApplicationDirectory}'");
                Directory.SetCurrentDirectory(currentApplicationDirectory);
            }

            // get computer info
            computerInfo = new ComputerInfo();

            // Full log path
            FileInfo fileInfo = new FileInfo(logPath);
            string logFullPath = fileInfo.FullName;
            //string logFullPath = GetAbsolutePath(logPath);

            // Read in settings file
            settings = new Settings(settingsFileName);

            // Initialize settings validator
            SettingsValidator settingsValidator = new SettingsValidator();


            var foo = new Uri("pack://siteoforigin:,,,/Config/logo.xaml", UriKind.RelativeOrAbsolute);
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = foo });


            // Parse Command Line arguments
            logger.Warn("Arguments Length: " + Environment.GetCommandLineArgs().Length);
            if (Environment.GetCommandLineArgs().Length == 1)
            {
                // Start application normally in UI mode
                logger.Warn("Application started without any switches. Running application in default interactive mode with user interface.");

                uiSession = true;

                // Validate settings object
                if (settings != null)
                {
                    // Change log file path if custom path is specified
                    SettingsItem logPathFromSettings = settings.GetSettingsItem("Other", "LogFilePath");
                    SettingsItemPathValidationResult logPathFromSettingsValidationResult = settingsValidator.ValidateSettingsItemPath(logPathFromSettings, false);

                    if(logPathFromSettingsValidationResult.ValidatedPath != logPath)
                    {
                        string oldLogPath = logPath;
                        string newLogPath = logPathFromSettingsValidationResult.ValidatedPath;

                        config = new NLog.Config.LoggingConfiguration();
                        logfile = new NLog.Targets.FileTarget("logfile") { FileName = newLogPath };
                        logconsole = new NLog.Targets.ConsoleTarget("logconsole");
                        config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
                        config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);
                        NLog.LogManager.Configuration = config;
                        logger = NLog.LogManager.GetCurrentClassLogger();
                    }
                } else
                {
                    logger.Error($"Failed to create Settings object.");
                    MessageBox.Show($"Unable to create Settings object. Either the '{settingsFileName}' file is missing or it has errors in its formatting. Check the log at '{logFullPath}' for more details.", $"Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Environment.Exit(Environment.ExitCode);
                }

                mainWindow = new MainWindow();
                mainWindow.ShowDialog();
            } else // Application was started with command line arguments
            {
                // attach application to the console
                AttachConsole(ATTACH_PARENT_PROCESS);  
                
                // Validate settings object
                if (settings != null)
                {
                    // Change log file path if custom path is specified
                    SettingsItem logPathFromSettings = settings.GetSettingsItem("Other", "LogFilePath");
                    SettingsItemPathValidationResult logPathFromSettingsValidationResult = settingsValidator.ValidateSettingsItemPath(logPathFromSettings, false);

                    if (logPathFromSettingsValidationResult.ValidatedPath != logPath)
                    {
                        string oldLogPath = logPath;
                        string newLogPath = logPathFromSettingsValidationResult.ValidatedPath;

                        config = new NLog.Config.LoggingConfiguration();
                        logfile = new NLog.Targets.FileTarget("logfile") { FileName = newLogPath };
                        logconsole = new NLog.Targets.ConsoleTarget("logconsole");
                        config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
                        config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);
                        NLog.LogManager.Configuration = config;
                        logger = NLog.LogManager.GetCurrentClassLogger();
                    }
                } else
                {
                    logger.Error($"Failed to create Settings object.");
                    Console.Error.WriteLine($"Error: Unable to create Settings object. Either the '{settingsFileName}' file is missing or it has errors in its formatting. Check the log at '{logFullPath}' for more details.", $"Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Environment.Exit(Environment.ExitCode);                    
                }

                foreach (var commandLineArg in Environment.GetCommandLineArgs())
                {
                    switch (commandLineArg)
                    {
                        case "-RunTaskSequence": // Attempts to launch specified Task Sequence
                            logger.Warn($"Application started with '-RunTaskSequence' switch. Running application in non-interactive mode.");
                            logger.Info($"The Operating System's OperatingSystemSKU is {computerInfo.OperatingSystemSKU}");

                            TaskSequenceRunner tsStarter = new TaskSequenceRunner();

                            if (computerInfo.OperatingSystemSKU == 4)
                            {
                                logger.Info("Choosing the Windows 10 Enterprise Task Sequence");
                                taskSequenceName = settings.GetSettingsItemValue("TaskSequence", "EntTSName");
                            } else if (computerInfo.OperatingSystemSKU == 121)
                            {
                                logger.Info("Choosing the Windows 10 Education Task Sequence");
                                taskSequenceName = settings.GetSettingsItemValue("TaskSequence", "EduTSName");
                            } else
                            {
                                logger.Error($"Unable to determine the appropriate Task Sequence to run based on the OS's OperatingSystemSKU. Failing out...");
                                tsStarter.FailGracefully($"Unable to determine the appropriate Task Sequence to run based on the OS's OperatingSystemSKU");
                                break;
                            }

                            logger.Info($"Calling RunTaskSquence({taskSequenceName})");                            
                            tsStarter.RunTaskSequence(taskSequenceName);
                            break;

                        case "-CleanUp": // Runs post Task Sequence cleanup
                            logger.Warn("Application started with '-CleanUp' switch. Running application in non-interactive mode.");
                            Cleanup cleanup = new Cleanup();
                            cleanup.RemoveLegalNoticeRegKeys();
                            cleanup.RemoveScheduledTask();
                            cleanup.RemoveUPGBackground();

                            logger.Warn("Finished '-CleanUp' procedures. Exiting.");
                            Environment.Exit(Environment.ExitCode);
                            break;
                        case "-TSFailure":
                            logger.Warn("Application started with '-TSFailure' switch. Running application in non-interactive mode.");
                            
                            // Set the Failure notice
                            LegalNoticeSetter legalNoticeSetter = new LegalNoticeSetter();
                            string tsFailureLegalNoticeCaption = settings.GetSettingsItemValue("PostGUI", "LegalNoticeCaptionOnTSFailure");
                            string tsFailureLegalNoticeText = settings.GetSettingsItemValue("PostGUI", "LegalNoticeTextOnTSFailure");
                            legalNoticeSetter.SetLegalNoticeRegKeys(tsFailureLegalNoticeCaption, tsFailureLegalNoticeText);

                            // Register the Cleanup scheduled task
                            ScheduledTaskRunner scheduledTaskRunner = new ScheduledTaskRunner();
                            string scheduledTaskValidatedPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "CleanupXmlPath"), true).ValidatedPath;
                            scheduledTaskRunner.RegisterScheduledTask(null, scheduledTaskValidatedPath);

                            logger.Warn("Finished '-TSFailure' procedures. Exiting.");
                            Environment.Exit(Environment.ExitCode);
                            break;
                    }
                }

            }
        }

        public static void ExecuteCommand(string command)
        {
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardOutput = true;
            process = Process.Start(processInfo);

            process.WaitForExit();
            var exitCode = process.ExitCode;
            process.Close();
            logger.Info("Command '" + command + "' exited with exit code: " + exitCode);
        }

        public static void RestartComputer()
        {
            ExecuteCommand("shutdown -r -t 0");
        }

        public static bool IsUiSession()
        {
            logger.Info($"Checking if the application is currently running in a UI session");
            if(mainWindow != null)
            {
                logger.Info($"The application is currently running in a UI session");
                return true;
            } else
            {
                logger.Error($"The application is not running in a UI session");
                return false;
            }
        }

        public static String GetAbsolutePath(String path)
        {
            return GetAbsolutePath(null, path);
        }

        public static String GetAbsolutePath(String basePath, String path)
        {
            if (path == null)
                return null;
            if (basePath == null)
                basePath = System.IO.Path.GetFullPath("."); // quick way of getting current working directory
            else
                basePath = GetAbsolutePath(null, basePath); // to be REALLY sure ;)
            String finalPath;
            // specific for windows paths starting on \ - they need the drive added to them.
            // I constructed this piece like this for possible Mono support.
            if (!System.IO.Path.IsPathRooted(path) || "\\".Equals(System.IO.Path.GetPathRoot(path)))
            {
                if (path.StartsWith(System.IO.Path.DirectorySeparatorChar.ToString()))
                    finalPath = System.IO.Path.Combine(System.IO.Path.GetPathRoot(basePath), path.TrimStart(System.IO.Path.DirectorySeparatorChar));
                else
                    finalPath = System.IO.Path.Combine(basePath, path);
            }
            else
                finalPath = path;
            // resolves any internal "..\" to get the true full path.
            return System.IO.Path.GetFullPath(finalPath);
        }

        /// <summary>
        /// Gets the full path of the given executable filename as if the user had entered this
        /// executable in a shell. So, for example, the Windows PATH environment variable will
        /// be examined. If the filename can't be found by Windows, null is returned.</summary>
        /// <param name="exeName"></param>
        /// <returns>The full path if successful, or null otherwise.</returns>
        public static string GetFullPathFromWindows(string exeName)
        {
            if (exeName.Length >= MAX_PATH)
                throw new ArgumentException($"The executable name '{exeName}' must have less than {MAX_PATH} characters.",
                    nameof(exeName));

            StringBuilder sb = new StringBuilder(exeName, MAX_PATH);
            return PathFindOnPath(sb, null) ? sb.ToString() : null;
        }

        // https://docs.microsoft.com/en-us/windows/desktop/api/shlwapi/nf-shlwapi-pathfindonpathw
        // https://www.pinvoke.net/default.aspx/shlwapi.PathFindOnPath
        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode, SetLastError = false)]
        static extern bool PathFindOnPath([In, Out] StringBuilder pszFile, [In] string[] ppszOtherDirs);

        // from MAPIWIN.h :
        private const int MAX_PATH = 260;
    }
}
