﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class PhysicalDisk
    {
        //public ulong AllocatedSize { get; set; }
        public ushort BusType { get; set; }
        public string DeviceId { get; set; }
        //public string FirmwareVersion { get; set; }
        public string FriendlyName { get; set; }
        //public ushort HealthStatus { get; set; }
        //public ulong LogicalSectorSize { get; set; }
        public ushort MediaType { get; set; }
        public string Model { get; set; }
        public string ObjectId { get; set; }
        //public ushort[] OperationalStatus { get; set; }
        public string PhysicalLocation { get; set; }
        //public ulong PhysicalSectorSize { get; set; }
        //public string SerialNumber { get; set; }
        //public ulong Size { get; set; }
        //public uint SpindleSpeed { get; set; }
        //public ushort[] SupportedUsages { get; set; }
        public string UniqueId { get; set; }
        //public ushort UniqueIdFormat { get; set; }
        //public ushort Usage { get; set; }
        //public ulong VirtualDiskFootprint { get; set; }
    }
}
