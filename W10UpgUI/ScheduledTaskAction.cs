﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    class ScheduledTaskAction
    {
        public string ActionType { get; set; }
        public string ActionCommand { get; set; }
        public string ActionArguments { get; set; }
        public string ActionWorkingDirectory { get; set; }
    }
}
