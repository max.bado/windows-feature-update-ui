﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class Volume
    {
        public string ObjectId { get; set; }
        public string UniqueId { get; set; }
        //public uint AllocationUnitSize { get; set; }
        //public uint DedupMode { get; set; }
        public char DriveLetter { get; set; }
        public uint DriveType { get; set; }
        public string FileSystem { get; set; }
        public string FileSystemLabel = null;
        public ushort FileSystemType { get; set; }
        //public ushort HealthStatus { get; set; }
        //public ushort[] OperationalStatus { get; set; }
        public string Path { get; set; }
        public ulong Size { get; set; }
        public ulong SizeRemaining { get; set; }
    }

}
