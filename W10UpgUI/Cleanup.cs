﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32.TaskScheduler;
using System.Diagnostics;
using Microsoft.Win32;

namespace W10UpgUI
{
    public class Cleanup
    {
        private Settings settings;
        SettingsValidator settingsValidator;
        ScheduledTaskRunner scheduledTaskRunner;
        public static NLog.Logger logger;

        public Cleanup()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            settings = App.settings;
            settingsValidator = new SettingsValidator();
            scheduledTaskRunner = new ScheduledTaskRunner();
        }

        public void RemoveScheduledTask()
        {
            // Remove Run Task Sequence Scheduled Task
            string runTsScheduledTaskPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "RunTSXmlPath"), true).ValidatedPath;
            scheduledTaskRunner.UnregisterScheduledTask(runTsScheduledTaskPath);

            // Remove Post Failure Cleanup Scheduled Task
            string cleanupScheduledTaskPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "CleanupXmlPath"), true).ValidatedPath;
            scheduledTaskRunner.UnregisterScheduledTask(cleanupScheduledTaskPath);

            //string scheduledTaskXMLPath = settings.GetSettingsItemValue("Scheduled", "ScheduledTaskXML");
            //string scheduledTaskName = settings.GetSettingsItemValue("Other", "ScheduledTaskName");
            //TaskService.Instance.RootFolder.DeleteTask(scheduledTaskName, false);
        }

        public void RemoveLegalNoticeRegKeys()
        {
            logger.Info($"Starting cleanup of Legal Notice registry keys.");
            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            }
            else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                string regKeyPath = @"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";
                using (RegistryKey regKey = regLocalMachine.OpenSubKey(regKeyPath, true))
                {
                    if(regKey.GetValue("legalnoticetext") != null)
                    {
                        logger.Warn($"Removing '{regKey.Name} | legalnoticetext'.");
                        regKey.DeleteValue("legalnoticetext");
                    } else
                    {
                        logger.Info($"'{regKey.Name} | legalnoticetext' does not exist. Nothing to remove.");
                    }
                    
                    if(regKey.GetValue("legalnoticecaption") != null)
                    {
                        logger.Warn($"Removing '{regKey.Name} | legalnoticecaption'.");
                        regKey.DeleteValue("legalnoticecaption");
                    } else
                    {
                        logger.Info($"'{regKey.Name} | legalnoticecaption' does not exist. Nothing to remove.");
                    }    
                }
            }
        }

        public void RemoveUPGBackground()
        {
            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            } else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider to retrieve uninstall keys
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                // Find uninstall keys for UPGBackground
                string displayName = "UPGBackground";
                string regUninstallKeyPath = @"SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
                using (RegistryKey regUninstallKey = regLocalMachine.OpenSubKey(regUninstallKeyPath))
                {
                    var regKeyList = from keyName in regUninstallKey.GetSubKeyNames()
                                     where (string)regLocalMachine.OpenSubKey($"{regUninstallKeyPath}\\\\{keyName}").GetValue("DisplayName") == displayName
                                     select regLocalMachine.OpenSubKey($"{regUninstallKeyPath}\\\\{keyName}");

                    if (regKeyList.Count() > 0)
                    {
                        foreach (RegistryKey regKey in regKeyList)
                        {
                            // Run the uninstall command to remove UPGBackground
                            string appGUID = regKey.Name.Split('\\').Last();
                            string uninstallCommand = $"msiexec.exe /x{appGUID} /qn";
                            logger.Info($"Running command line '{uninstallCommand}' to uninstall {displayName}.");
                            App.ExecuteCommand(uninstallCommand);
                        }
                    }
                    else
                    {
                        logger.Warn($"{displayName} does not appear to be installed.");
                    }
                }
            }
        }
    }
}
