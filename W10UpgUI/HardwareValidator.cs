﻿using Microsoft.Management.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class HardwareValidator
    {
        private ComputerInfo computerInfo;
        private Settings settings;
        private ErrorHandler errorHandler;
        private ulong currentFreeSpaceBytes;
        private ulong totalPhysicalMemory;
        private bool spaceCheckPassed;
        private bool memoryCheckPassed;
        private bool powerCheckPassed;
        private bool usbAttachedCheckPassed;
        private bool networkCheckPassed;
        private bool bitlockerCheckPassed;
        private string ethernetDomainName;
        private string helpdeskName;
        private string helpdeskNumber;
        private string helpdeskEmail;
        private string helpdeskNotice;

        // Logging
        private static NLog.Logger logger;


        public HardwareValidator()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            computerInfo = App.computerInfo;
            settings = App.settings;
            errorHandler = new ErrorHandler();
            this.ethernetDomainName = settings.GetSettingsItemValue("Network", "EthernetDomainName");
            this.helpdeskName = settings.GetSettingsItemValue("Customizations", "HelpdeskName");
            this.helpdeskNumber = settings.GetSettingsItemValue("Customizations", "HelpdeskNumber");
            this.helpdeskEmail = settings.GetSettingsItemValue("Customizations", "HelpdeskEmail");
            this.helpdeskNotice = $"Please call the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}";
        }

        // START Hardware Check methods
        // Checks if all Hardware Checks have Passed
        public int HardwareChecksPassed()
        {
            //logger.Warn($"Begin Hardware Checks Validation.");
            int hardwareChecksPassedResult = 0; // 0 = Memory Check Failed; 1 = All Passed; 2 = Non-critical fail

            //logger.Info($"MemoryCheck: {memoryCheckPassed}");
            //logger.Info($"USBAttachedCheck: {usbAttachedCheckPassed}");
            //logger.Info($"NetworkCheck: {networkCheckPassed}");
            //logger.Info($"SpaceCheck: {spaceCheckPassed}");
            //logger.Info($"PowerCheck: {powerCheckPassed}");
            //logger.Info($"BitlockerCheck: {bitlockerCheckPassed}");

            if (memoryCheckPassed == false)
            {
                // Fail if Memory too low
                //logger.Error($"Memory check failed");
                hardwareChecksPassedResult = 0;

            }
            else if ((spaceCheckPassed == true) && (usbAttachedCheckPassed == true) && (networkCheckPassed == true) && (memoryCheckPassed == true) && (powerCheckPassed == true) /*&& (bitlockerCheckPassed == true) */)
            {
                // Pass if all passed
                //logger.Info($"All passed");
                hardwareChecksPassedResult = 1;
            }
            else
            {
                // Non-critical failure
                //logger.Warn($"Non-critical hardware validation failure");
                hardwareChecksPassedResult = 2;
            }

            //logger.Warn($"End Hardware Checks Validation.");
            return hardwareChecksPassedResult;
    }

        // Checks if machine meets Space requirement
        public bool CheckSpace(List<Volume> volumes, ulong minimumSpaceGB)
        {
            logger.Info($"Start OS volume space check");
            bool checkSpaceResult = false;

            // convert minimumSpaceGB to bytes
            ulong minimumSpaceBytes = minimumSpaceGB * 1024 * 1024 * 1024;

            // get list of volumes
            //List<Volume> volumeList = GetVolumes();

            if (volumes.Count() > 0)
            {
                logger.Info($"Found {volumes.Count()} volumes");
                string systemDriveLetter = computerInfo.SystemDrive.Substring(0, 1); // SystemDrive is in the "C:" format, so it's necessary to take the substring

                foreach (Volume volume in volumes)
                {                    
                    string driveLetter = volume.DriveLetter.ToString();
                    logger.Info($"Working on volume with drive letter '{driveLetter}'");

                    if (driveLetter == systemDriveLetter)
                    {
                        logger.Info($"Volume drive letter matches the OS drive letter. Proceeding...");
                        currentFreeSpaceBytes = volume.SizeRemaining;

                        if (currentFreeSpaceBytes >= minimumSpaceBytes)
                        {
                            logger.Info($"OS volume space check SUCCESS: Volume '{driveLetter}' has {currentFreeSpaceBytes} bytes of free space which is greater than the minimum required space ({minimumSpaceBytes} bytes)");
                            checkSpaceResult = true;
                            spaceCheckPassed = true;
                        }
                        else
                        {
                            logger.Error($"OS volume space check FAILURE: Volume '{driveLetter}' has {currentFreeSpaceBytes} bytes of free space which is less than the minimum required space ({minimumSpaceBytes} bytes)");
                            checkSpaceResult = false;
                            spaceCheckPassed = false;
                        }
                    } else
                    {
                        logger.Warn($"Volume drive letter does not match the OS drive letter. Skipping...");
                    }
                }
            } else
            {
                // No volumes detected
                string errorLogText = $"Unable to find a list of storage volumes";
                string errorNoticeText1 = $"The application ran into an error while checking volume space";
                string errorNoticeText2 = helpdeskNotice;
                errorHandler.ThrowCriticalError(errorLogText, errorNoticeText1, errorNoticeText2);
            }

            return checkSpaceResult;
        }

        // Checks if machine meets Memory requirement
        public bool CheckMemory(ulong minimumRamGb)
        {
            logger.Info($"Start memory check");
            bool checkMemoryResult = false;

            // convert minimumSpaceGB to bytes
            ulong minimumRamBytes = minimumRamGb * 1024 * 1024 * 1024;
            totalPhysicalMemory = computerInfo.TotalPhysicalMemory;

            if (totalPhysicalMemory >= minimumRamBytes)
            {
                logger.Info($"Memory check SUCCESS: The computer has {totalPhysicalMemory} bytes of RAM which is greater than the minimum required RAM ({minimumRamBytes} bytes)");
                checkMemoryResult = true;
                memoryCheckPassed = true;
            }
            else
            {
                logger.Error($"Memory check FAILURE: The computer has {totalPhysicalMemory} bytes of RAM which is less than the minimum required RAM ({minimumRamBytes} bytes)");
                checkMemoryResult = false;
                memoryCheckPassed = false;
            }

            return checkMemoryResult;
        }

        // Checks if Power is plugged in
        public int CheckPower(List<Battery> batteries)
        {
            logger.Info($"Start power check");
            int checkPowerStatus = 0; // 0 = Not a laptop, power connected; 1 = Laptop, plugged in; 2 = Laptop, unplugged; 3 = Laptop, no battery;

            bool isLaptop = IsLaptop();

            if (isLaptop)
            {
                if (batteries.Count > 0)
                {
                    foreach (Battery battery in batteries)
                    {
                        if (battery.BatteryStatus == 2)
                        {
                            // Laptop, plugged in...
                            checkPowerStatus = 1;
                            powerCheckPassed = true;
                            logger.Info($"Power check SUCCESS: Laptop computer is plugged in. Returning a result status code of '{checkPowerStatus}'");
                        }
                        else
                        {
                            // Laptop, unplugged...
                            checkPowerStatus = 2;
                            powerCheckPassed = false;
                            logger.Error($"Power check FAILURE: Laptop computer is unplugged. Returning a result status code of '{checkPowerStatus}'");
                        }
                    }
                }
                else
                {
                    // Laptop, no battery...
                    checkPowerStatus = 3;
                    powerCheckPassed = true;
                    logger.Warn($"Power check WARNING: Laptop computer does not have a battery. Returning a result status code of '{checkPowerStatus}'");
                }

            }
            else
            {
                // Not a laptop
                checkPowerStatus = 0;
                powerCheckPassed = true;
                logger.Info($"Power check SUCCESS: Computer is not a laptop. Returning a result status code of '{checkPowerStatus}'");
            }

            return checkPowerStatus;
        }

        // Checks Network connectivity
        public int CheckNetwork(List<NetAdapter> netAdapters)
        {
            logger.Info($"Start network check");
            int checkNetworkStatus = 0; // 0 = Domain Ethernet Check Passed; 1 = No Domain Ethernet Found; 2 = No Ethernet Found; 3 = No Network Connections;

            if (netAdapters.Count > 0)
            {
                logger.Info($"Found {netAdapters.Count()} network adapters");

                // check for ethernet connectivity
                switch (CheckActiveDomainEthernetConnection(netAdapters))
                {
                    case "DomainEthernetCheckPassed":
                        // Success
                        checkNetworkStatus = 0;
                        networkCheckPassed = true;
                        logger.Info($"Network check SUCCESS: Computer is connected to a domain network with an ethernet connection. Returning a result status code of '{checkNetworkStatus}'");
                        break;
                    case "NoDomainEthernetFound":
                        // No Domain Ethernet Found
                        checkNetworkStatus = 1;
                        networkCheckPassed = false;
                        logger.Error($"Network check FAILURE: Computer is connected to a NON-DOMAIN network with an ethernet connection. Returning a result status code of '{checkNetworkStatus}'");                 
                        break;
                    case "NoEthernetFound":
                        // No Ethernet Found
                        checkNetworkStatus = 2;
                        networkCheckPassed = false;
                        logger.Error($"Network check FAILURE: Computer is not connected with an ethernet connection. Returning a result status code of '{checkNetworkStatus}'");
                        break;
                }
            }
            else
            {
                // No Network Adapters detected
                checkNetworkStatus = 3;
                networkCheckPassed = false;
                string errorLogText = $"Unable to find a list of network adapters";
                string errorNoticeText1 = $"The application ran into an error while checking network";
                string errorNoticeText2 = helpdeskNotice;
                errorHandler.ThrowCriticalError(errorLogText, errorNoticeText1, errorNoticeText2);
            }

            return checkNetworkStatus;
        }

        // Check for attached USB storage
        public int CheckUsbAttached(List<Disk> disks)
        {
            logger.Info($"Start USB storage check");
            int checkUsbAttachedResult = 0;

            /*
            BusType Enumeration
            0   Unknown
            1   SCSI
            2   ATAPI
            3   ATA
            4   1394
            5   SSA
            6   Fibre Channel
            7   USB
            8   RAID
            9   iSCSI
            10  SAS
            11  SATA
            12  SD
            13  MMC
            14  Virtual
            15  File Backed Virtual
            16  Spaces
            17  NVMe
            18  SCM
            19  UFS
            */

            if (disks.Count > 0)
            {
                var usbDrives = from x in disks
                                where (x.BusType == 7 && x.IsOffline == false && x.OperationalStatus.Any(value => value != 53266))
                                select x;

                if (usbDrives.Count() == 0)
                {
                    // if success...
                    checkUsbAttachedResult = 0;
                    usbAttachedCheckPassed = true;
                    logger.Info($"USB storage check SUCCESS: No USB storage drives are currently attached to the computer");
                }
                else
                {
                    checkUsbAttachedResult = usbDrives.Count();
                    usbAttachedCheckPassed = false;
                    logger.Error($"USB storage check FAILURE: There are currently {usbDrives.Count()} USB drives attached to the computer");
                }
            } else
            {
                // throw new ArgumentOutOfRangeException("Disks count cannot be equal to or less than 0");
                usbAttachedCheckPassed = false;
                checkUsbAttachedResult = -1;
            }

            return checkUsbAttachedResult;
        }

        // Checks if an active domain ethernet connection is present
        private string CheckActiveDomainEthernetConnection(List<NetAdapter> netAdapterList)
        {
            logger.Info($"Start active domain Ethernet connection check");

            string checkActiveDomainEthernetConnectionResult;

            var netAdapterQuery = from netAdapter in netAdapterList
                                  where ((netAdapter.InterfaceType == 6) && (netAdapter.HardwareInterface == true) && (netAdapter.MediaConnectState == 1))
                                  select netAdapter;

            if (netAdapterQuery.Count() > 0)
            {
                logger.Info($"Found {netAdapterQuery.Count()} active network adapters");
                checkActiveDomainEthernetConnectionResult = "NoDomainEthernetFound";

                foreach (NetAdapter netAdapter in netAdapterQuery)
                {
                    logger.Info($"Checking network adapter '{netAdapter.Name}'");
                    if ((netAdapter.DNSDomain == this.ethernetDomainName))
                    {
                        checkActiveDomainEthernetConnectionResult = "DomainEthernetCheckPassed";
                        logger.Info($"Active domain ethernet connection check SUCCESS: Ethernet network adapter '{netAdapter.Name}' is connected to a domain network");
                        break;
                    }
                }
            }
            else
            {
                logger.Error($"Active domain ethernet connection check FAILURE: Unable to find any Ethernet network adapters");
                checkActiveDomainEthernetConnectionResult = "NoEthernetFound";
            }

            return checkActiveDomainEthernetConnectionResult;
        }

        // Checks if machine is a laptop
        public bool IsLaptop()
        {
            bool checkLaptopResult = false;

            foreach (ushort chassisType in computerInfo.ChassisTypes)
            {
                if ((chassisType == 9) || (chassisType == 10) || (chassisType == 14))
                {
                    checkLaptopResult = true;
                    break;
                }
            }

            return checkLaptopResult;
        }

        // Check if drive is Bitlocker protected
        public int CheckBitlockerProtection(EncryptableVolume encryptableVolume)
        {
            int checkBitlockerProtectionStatus= 0;
            switch (encryptableVolume.ConversionStatus)
            {
                case 0: // volume is fully decrypted 
                    checkBitlockerProtectionStatus = 0;
                    bitlockerCheckPassed = true;
                    break;
                case 1: // volume is fully encrypted
                    checkBitlockerProtectionStatus = 1;
                    bitlockerCheckPassed = true;
                    break;
                case 2: // encryption in progress
                    checkBitlockerProtectionStatus = 2;
                    bitlockerCheckPassed = false;
                    break;
                case 3: // decryption in progress
                    checkBitlockerProtectionStatus = 3;
                    bitlockerCheckPassed = false;
                    break;
                case 4: // encryption paused
                    checkBitlockerProtectionStatus = 4;
                    bitlockerCheckPassed = false;
                    break;
                case 5: // decryption paused
                    checkBitlockerProtectionStatus = 5;
                    bitlockerCheckPassed = false;
                    break;

            }
            return checkBitlockerProtectionStatus;
        }

        // END Hardware Check Methods

    }
}
