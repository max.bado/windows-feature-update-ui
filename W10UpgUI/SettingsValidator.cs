﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace W10UpgUI
{
    class SettingsValidator
    {
        private Settings settings;
        public static NLog.Logger logger;


        public SettingsValidator() {
            settings = App.settings;
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public string ValidateScheduledTaskXml(string scheduledTaskXmlPath)
        {
            logger.Info($"Validating Task Scheduler Xml located at '{scheduledTaskXmlPath}'");
            string scheduledTaskValidationStatus = "Valid";
            //bool scheduledTaskModifiedFlag = false;

            //List<ScheduledTaskAction> scheduledTaskActions = new List<ScheduledTaskAction>();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(scheduledTaskXmlPath);
            XmlNode root = xmlDocument.DocumentElement;
            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmlDocument.NameTable);
            xmlnsManager.AddNamespace("def", "http://schemas.microsoft.com/windows/2004/02/mit/task");
            XmlNode xmlNodeList = root.SelectSingleNode(@"/def:Task/def:Actions", xmlnsManager);

            if (xmlNodeList != null)
            {
                //scheduledTaskDetails = new ScheduledTaskDetails();
                XmlNodeList xmlChildNodes = xmlNodeList.ChildNodes;
                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    //ScheduledTaskAction scheduledTaskAction = new ScheduledTaskAction();
                    XmlNode commandNode = xmlNode["Command"];
                    XmlNode argumentsNode = xmlNode["Arguments"];
                    XmlNode workingDirectoryNode = xmlNode["WorkingDirectory"];

                    string scheduledTaskCommandPath = null;
                    string scheduledTaskCommandValidatedPath = null;
                    string scheduledTaskArguments = null;
                    string scheduledTaskWorkingDirectoryPath = null;

                    if (workingDirectoryNode != null)
                    {
                        scheduledTaskWorkingDirectoryPath = workingDirectoryNode.InnerText.Trim();
                        if (Directory.Exists(scheduledTaskWorkingDirectoryPath)) // Directory exists
                        {
                            logger.Info($"Scheduled Task working directory path: {scheduledTaskWorkingDirectoryPath}");
                        }
                        else // Can't find directory
                        {
                            logger.Error($"Unable to find Scheduled Task working directory path: {scheduledTaskWorkingDirectoryPath}");
                            scheduledTaskValidationStatus = "Working Directory not found";
                            return scheduledTaskValidationStatus;
                        }
                    }

                    if (commandNode != null)
                    {
                        scheduledTaskCommandPath = commandNode.InnerText.Trim();
                        logger.Info($"Discovered scheduled task command path: '{scheduledTaskCommandPath}'");

                        // Expand any environment variables in the path
                        logger.Info($"Attempting to expand any environment variables...");
                        scheduledTaskCommandPath = Environment.ExpandEnvironmentVariables(scheduledTaskCommandPath);
                        logger.Info($"Path post variable expansion: '{scheduledTaskCommandPath}'");

                        // Check if the file exists
                        logger.Info($"Checking if '{scheduledTaskCommandPath}' exists in the current context.");

                        if (File.Exists(scheduledTaskCommandPath)) // File exists
                        {
                            logger.Info($"'{scheduledTaskCommandPath}' exists.");
                            scheduledTaskCommandValidatedPath = scheduledTaskCommandPath;
                        }
                        else // File does not exist... Check if Windows knows where the command file is located 
                        {
                            logger.Warn($"'{scheduledTaskCommandPath}' does not exist in the current context...");
                            logger.Info($"Checking if '{scheduledTaskCommandPath}' can be resolved to a full path by the OS.");
                            scheduledTaskCommandValidatedPath = App.GetFullPathFromWindows(scheduledTaskCommandPath);

                            if (scheduledTaskCommandValidatedPath != null) // Windows knows where this file is
                            {
                                logger.Info($"Windows resolved file path for '{scheduledTaskCommandPath}' to the full path '{scheduledTaskCommandValidatedPath}'");

                            }
                            else // Windows does not know where this file is...
                            {
                                logger.Warn($"Windows was NOT able to resolve the file path for '{scheduledTaskCommandPath}'");

                                // Check if a working directory is provided and then test to see if the command file lives in there
                                if (scheduledTaskWorkingDirectoryPath != null) // Working directory was provided
                                {
                                    logger.Info($"The following working directory is provided: '{scheduledTaskWorkingDirectoryPath}'");

                                    // full path = working directory + command file path
                                    scheduledTaskCommandValidatedPath = System.IO.Path.Combine(scheduledTaskWorkingDirectoryPath, scheduledTaskCommandPath);
                                    logger.Info($"Checking if the command file exists in the following location: '{scheduledTaskCommandValidatedPath}'");

                                    if (File.Exists(scheduledTaskCommandValidatedPath)) // Command path is valid
                                    {
                                        logger.Info($"The command file was found at the following location: '{scheduledTaskCommandValidatedPath}'");
                                    }
                                    else // Command path is not valid. Try testing absolute path
                                    {
                                        logger.Warn($"The command file was NOT found at the following location: '{scheduledTaskCommandValidatedPath}'");

                                        //scheduledTaskCommandValidatedPath = App.GetAbsolutePath(scheduledTaskCommandPath);
                                        FileInfo fileInfo = new FileInfo(scheduledTaskCommandPath);
                                        scheduledTaskCommandValidatedPath = fileInfo.FullName;
                                        logger.Warn($"Checking if the file exists at '{scheduledTaskCommandValidatedPath}'");

                                        if (fileInfo.Exists)  // Command file exists at the absolute path
                                        {
                                            logger.Info($"The command file was found at the absolute path: '{scheduledTaskCommandValidatedPath}'");
                                        }
                                        else // Command file does not exist at the absolute path
                                        {
                                            logger.Error($"The command file was NOT found at the absolute path: '{scheduledTaskCommandValidatedPath}'. Unable to continue");
                                            scheduledTaskCommandValidatedPath = null;
                                            scheduledTaskValidationStatus = "Command file path not found";
                                            return scheduledTaskValidationStatus;
                                        }
                                    }
                                }
                                else // No working directory, so we need to check if the command path can be resolved by its absolute path to the current directory
                                {
                                    FileInfo fileInfo = new FileInfo(scheduledTaskCommandPath);
                                    scheduledTaskCommandValidatedPath = fileInfo.FullName;
                                    //scheduledTaskCommandValidatedPath = App.GetAbsolutePath(scheduledTaskCommandPath);
                                    logger.Warn($"A working directory was not provided for the file '{scheduledTaskCommandPath}'. Checking if the file exists at '{scheduledTaskCommandValidatedPath}'");

                                    if (File.Exists(scheduledTaskCommandValidatedPath))  // Command file exists at the absolute path
                                    {
                                        logger.Info($"The command file was found at the absolute path: '{scheduledTaskCommandValidatedPath}'");
                                    }
                                    else // Command file does not exist at the absolute path
                                    {
                                        logger.Error($"The command file was NOT found at the absolute path: '{scheduledTaskCommandValidatedPath}'. Unable to continue");
                                        scheduledTaskCommandValidatedPath = null;
                                        scheduledTaskValidationStatus = "Command file path not found";
                                        return scheduledTaskValidationStatus;
                                    }
                                }
                            }
                        }
                        /*
                        if(scheduledTaskCommandPath != scheduledTaskCommandValidatedPath)
                        {
                            scheduledTaskModifiedFlag = true;
                        }
                        */
                    }

                    if (argumentsNode != null)
                    {
                        scheduledTaskArguments = argumentsNode.InnerText.Trim();
                        logger.Info($"Scheduled Task command arguments: {scheduledTaskArguments}");
                    }


                    //scheduledTaskAction.ActionCommand = scheduledTaskCommandValidatedPath;
                    //scheduledTaskAction.ActionArguments = scheduledTaskArguments;
                    //scheduledTaskAction.ActionWorkingDirectory = scheduledTaskWorkingDirectoryPath;

                    // Add the scheduledTaskAction object to the list
                    //scheduledTaskActions.Add(scheduledTaskAction);
                }

                //scheduledTaskDetails.ScheduledTaskActions = scheduledTaskActions;
                //scheduledTaskDetails.ScheduledTaskModifiedFlag = scheduledTaskModifiedFlag;
            }

            return scheduledTaskValidationStatus;
        }

        public SettingsItemPathValidationResult ValidateSettingsItemPath(SettingsItem settingsItem, bool exists)
        {
            logger.Info($"Begin settings item path validation:");
            logger.Info($"Settings Item Group: {settingsItem.ItemGroup}");
            logger.Info($"Settings Item Name: {settingsItem.ItemName}");
            logger.Info($"Settings Item Value: {settingsItem.ItemValue}");

            SettingsItemPathValidationResult result = new SettingsItemPathValidationResult();
            result.Result = false;
            result.SettingsItem = settingsItem;

            logger.Info($"Validating path for item '{settingsItem.ItemName}'");
            string settingsItemPath = settingsItem.ItemValue;
            if (settingsItemPath != null)
            {
                // Expand any environment variables in the path
                logger.Info($"Attempting to expand any environment variables...");
                settingsItemPath = Environment.ExpandEnvironmentVariables(settingsItemPath);
                logger.Info($"Path post variable expansion: '{settingsItemPath}'"); 

                FileInfo fileInfo = new FileInfo(settingsItemPath);
                DirectoryInfo directoryInfo = new DirectoryInfo(settingsItemPath);

                // If we're not verifying existence, simply return the full path
                if(!exists)
                {
                    // Settings item file path validated
                    logger.Warn($"'exists' parameter is set to false. Skipping verification of whether the item currently exists.");
                    logger.Info($"'{settingsItem.ItemName}' path resolved to file '{fileInfo.FullName}'");
                    result.ValidatedPath = fileInfo.FullName;
                    result.Result = true;
                    return result;
                }

                // Check if the path is a file path
                fileInfo = new FileInfo(settingsItemPath);
                if (fileInfo.Exists)
                {
                    // Settings item file path validated
                    logger.Info($"'{settingsItem.ItemName}' path resolved to file '{fileInfo.FullName}'");
                    result.ValidatedPath = fileInfo.FullName;
                    result.Result = true;
                    return result;
                }

                // Check if the path is a directory path
                directoryInfo = new DirectoryInfo(settingsItemPath);
                if(directoryInfo.Exists)
                {
                    // Settings item directory path validated
                    logger.Info($"'{settingsItem.ItemName}' path resolved to directory '{directoryInfo.FullName}'");
                    result.ValidatedPath = directoryInfo.FullName;
                    result.Result = true;
                    return result;
                }

                // If we got this far, settings item path validation failed
                logger.Error($"Unable to find '{settingsItem.ItemName}' file/directory at specified path: ''{settingsItem.ItemValue}''");
            }

            return result;
        }

        public bool ValidateLogSharePath()
        {
            bool result = false;
            // Validate LogShare Path
            logger.Info($"Validating LogSharePath.");
            string logSharePath = this.settings.GetSettingsItemValue("Other", "LogSharePath");
            if (logSharePath == "")
            {
                logger.Info($"LogSharePath is empty. Logs won't be copied to remote share.");
                result = true;
            }
            else if (logSharePath != null)
            {
                if (Directory.Exists(logSharePath))
                {
                    // LogSharePath Validated
                    logger.Info($"LogSharePath exists at: '{logSharePath}'");
                    result = true;
                }
                else
                {
                    // LogSharePath Validation failed
                    logger.Info($"Unable to find LogSharePath at specified path: '{logSharePath}'");
                }
            }

            return result;
        }
    }
}
