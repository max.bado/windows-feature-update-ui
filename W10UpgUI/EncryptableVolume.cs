﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class EncryptableVolume
    {
        /*
        Possible Enumeration Values for ConversionStatus: 
        0 - FullyDecrypted
        1 - FullyEncrypted
        2 - EncryptionInProgress
        3 - DecryptionInProgress
        4 - EncryptionPaused
        5 - DecryptionPaused
        */
        public uint ConversionStatus { get; set; } 
        public string DeviceId { get; set; }
        public string DriveLetter { get; set; }
        public uint EncryptionMethod { get; set; }
        public bool IsVolumeInitializedForProtection { get; set; }
        public uint LockStatus { get; set; } // 0 - Unlocked; 1 - Locked
        //public string PersistentVolumeID { get; set; }
        public uint ProtectionStatus { get; set; } // 0 - Protection Off; 1 - Protection On; 2 - Protection Unknown 
        public uint VolumeType { get; set; } // 0 - OSVolume; 1 - FixedDataVolume; 2 - PortableDataVolume
    }
}
