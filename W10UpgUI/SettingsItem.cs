﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class SettingsItem
    {
        public string ItemGroup { get; set; }
        public string ItemName { get; set; }
        public string ItemValue { get; set; }
    }
}
