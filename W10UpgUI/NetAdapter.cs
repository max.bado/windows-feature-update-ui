﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class NetAdapter
    {
        public bool ConnectorPresent { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string DriverDescription { get; set; }
        public string DriverName { get; set; }
        public string DriverProvider { get; set; }
        public string DriverVersionString { get; set; }
        public ushort EnabledState { get; set; }
        public bool HardwareInterface { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceDescription { get; set; }
        public uint InterfaceType { get; set; }
        public uint MediaConnectState { get; set; }
        public string Name { get; set; }
        public string[] NetworkAddresses { get; set; }
        public string PermanentAddress { get; set; }
        public string PnPDeviceID { get; set; }
        public uint State { get; set; }

        public string MACAddress { get; set; }
        public string[] IPAddress { get; set; }
        public string DNSDomain { get; set; }
        public string DHCPServer { get; set; }
        public string[] DefaultIPGateway { get; set; }
        public string[] IPSubnet { get; set; }
    }
}
