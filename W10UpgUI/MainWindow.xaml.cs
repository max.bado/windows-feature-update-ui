﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Microsoft.Management.Infrastructure;
using System.Threading;
using Newtonsoft.Json.Linq;
using MahApps.Metro.Controls.Dialogs;
using System.Windows.Threading;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO;
using Microsoft.Win32.TaskScheduler;
using System.Xml;
using System.Runtime.InteropServices;
using System.Management;
using System.Windows.Markup;
using System.Collections;

namespace W10UpgUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private ComputerInfo computerInfo;
        private Settings settings;
        private HardwareValidator hardwareValidator;
        private ErrorHandler errorHandler;
        private int currentTabIndex;
        private bool spaceCheckPassed;
        private bool memoryCheckPassed;
        private bool powerCheckPassed;
        private bool networkCheckPassed;
        private int usbAttachedCheckPassed;
        private bool bitlockerCheckPassed;
        private bool hardwareChecksPassed;

        // private Style logo;

        private int restartMinutes;
        private DataGatherer dataGatherer;
        private DispatcherTimer countdownTimer;
        private TimeSpan restartTimeSpan;
        private int restartSeconds;
        private ScheduledTaskRunner scheduledTaskRunner;
        private LegalNoticeSetter legalNoticeSetter;
        private SettingsValidator settingsValidator;
        private DataWatcher dataWatcher;

        //private string logSharePath;
        private string runTSXmlPath;
        //private string cleanupOnTSStartFailureXmlPath;
        //private string scheduledTaskXmlFullPath;
        private string upgBackgroundDirectoryPath;
        //private string scheduledTaskCommand;
        //private string scheduledTaskCommandFullPath;
        //private string scheduledTaskArguments;
        private string legalNoticeCaption;
        private string legalNoticeText;
        private string legalNoticeCaptionOnStartFailure;
        private string legalNoticeTextOnStartFailure;
        public string helpdeskNumber;
        public string helpdeskName;
        public string helpdeskEmail;

        private bool topmost;
        private int upgradeBuildVersion;

        // private List<PhysicalDisk> physicalDisks;
        private List<Volume> volumes;
        private List<Battery> batteries;
        private List<NetAdapter> netAdapters;
        private List<EncryptableVolume> encryptableVolumes;
        private List<Disk> disks;

        // Logging
        private static NLog.Logger logger;


        public MainWindow()
        {
            InitializeComponent();

            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            // Initialize helper components
            computerInfo = App.computerInfo;
            settings = App.settings;
            hardwareValidator = new HardwareValidator();
            scheduledTaskRunner = new ScheduledTaskRunner();
            legalNoticeSetter = new LegalNoticeSetter();
            dataGatherer = new DataGatherer();
            settingsValidator = new SettingsValidator();
            dataWatcher = new DataWatcher();
            errorHandler = new ErrorHandler();

            // Assign actions to buttons
            // Intro Tab buttons
            ButtonLeft1.Click += CloseApp_Click;                // Exit
            ButtonRight1.Click += GoToChecksTab_Click;          // Advance to Checks Tab

            // Checks Tab buttons
            ButtonLeft2.Click += GoToIntroTab_Click;            // Return to Intro Tab
            ButtonRight2.Click += GoToReadyToUpdateTab_Click;   // Advance to Ready To Update Tab
            ButtonRight7.Click += CloseApp_Click;               // Post check failure "exit" button

            // Remediate Space Tab buttons
            ButtonLeft3.Click += CloseApp_Click;                // Exit
            ButtonRight3.Click += GoToReadyToUpdateTab_Click;   // Advance to Ready To Update Tab

            // Insufficient Space Tab buttons
            ButtonRight5.Click += CloseApp_Click;               // Exit

            // Insufficient Memory Tab buttons
            ButtonRight6.Click += CloseApp_Click;               // Exit

            // Ready To Update Tab buttons
            ButtonLeft4.Click += GoToChecksTab_Click;           // Go back to Ready To Update Tab
            ButtonRight4.Click += StartUpdate_Click;            // Exit

            // Set variables according to the JSON settings file
            legalNoticeCaption = settings.GetSettingsItemValue("PostGUI", "LegalNoticeCaption");
            legalNoticeText = settings.GetSettingsItemValue("PostGUI", "LegalNoticeText");
            legalNoticeCaptionOnStartFailure = settings.GetSettingsItemValue("PostGUI", "LegalNoticeCaptionOnStartFailure");
            legalNoticeTextOnStartFailure = settings.GetSettingsItemValue("PostGUI", "LegalNoticeTextOnStartFailure");
            helpdeskEmail = settings.GetSettingsItemValue("Customizations", "HelpdeskEmail");
            helpdeskName = settings.GetSettingsItemValue("Customizations", "HelpdeskName");
            helpdeskNumber = settings.GetSettingsItemValue("Customizations", "HelpdeskNumber");
            runTSXmlPath = settings.GetSettingsItemValue("ScheduledTask", "RunTSXmlPath");
            topmost = bool.Parse(settings.GetSettingsItemValue("Customizations", "Topmost"));
            upgradeBuildVersion = int.Parse(settings.GetSettingsItemValue("TaskSequence", "UpgradeBuildVersion"));


            // Set the window to topmost if specified
            if (topmost)
            {
                this.Topmost = topmost;
            }

            // Quit if the current build is the same or greater than the task sequence build
            if (computerInfo.BuildNumber >= upgradeBuildVersion)
            {
                logger.Error($"The current OS build, {computerInfo.BuildNumber}, is greater than or equal to the Task Sequence build, {upgradeBuildVersion}. Stopping.");
                this.MetroWindow_Notice($"Windows 10 is already up-to-date!", $"If you have questions or concerns, please contact the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}.");
            }
            else
            {
                logger.Info($"The current OS build, {computerInfo.BuildNumber}, is less than the Task Sequence build, {upgradeBuildVersion}. Continuing.");
            }

            // Validate settings
            var backgroundThread50 = new Thread(() => {
                while (App.mainWindow == null)
                {
                    logger.Info($"Waiting for the main window to initialize...");
                    Thread.Sleep(200);
                }
                logger.Info($"Main window initialized. Validating settings...");

                // We need to use the dispatcher here since the main window is owned by a different thread
                this.Dispatcher.Invoke(() =>
                {
                    ValidateSettings();
                });

            });
            backgroundThread50.Start();

            // Show UI elements for Intro Tab
            GoToIntroTab();
        }

        // iterate through a window's objects
        public static IEnumerable<T> FindLogicalChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                foreach (object rawChild in LogicalTreeHelper.GetChildren(depObj))
                {
                    if (rawChild is DependencyObject)
                    {
                        DependencyObject child = (DependencyObject)rawChild;
                        if (child is T)
                        {
                            yield return (T)child;
                        }

                        foreach (T childOfChild in FindLogicalChildren<T>(child))
                        {
                            yield return childOfChild;
                        }
                    }
                }
            }
        }

        public void OnLoad(object sender, RoutedEventArgs e)
        {
            UpdateUiText();
        }

        private void UpdateUiText()
        {
            // Update UI with text variables            
            foreach (TextBlock tb in FindLogicalChildren<TextBlock>(this))
            {
                // do something with tb here
                string newText = tb.Text;
                // logger.Info($"Processing TextBlock: {tb.Text}");

                newText = newText.Replace("##HelpdeskName##", helpdeskName);
                newText = newText.Replace("##HelpdeskNumber##", helpdeskNumber);
                newText = newText.Replace("##HelpdeskEmail##", helpdeskEmail);

                if (tb.Text != newText)
                {                    
                    tb.Text = newText;
                    logger.Info($"Updating TextBlock: {tb.Text}");
                }
            }
        }

        private void GoToIntroTab()
        {

            ShowIntroTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Visible;
            ButtonLeft1.Visibility = Visibility.Visible;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Items
            RestartBox.Visibility = Visibility.Hidden;
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;
        }

        // Intro Tab button events
        private void GoToChecksTab_Click(object sender, RoutedEventArgs e)
        {
            ShowChecksTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Hidden;
            ButtonLeft1.Visibility = Visibility.Hidden;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Visible;
            ButtonLeft2.Visibility = Visibility.Visible;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;


            // check if USB storage is attached
            var backgroundThread1 = new Thread(() => {
                WatchForUsbChanges();
            });
            backgroundThread1.Start();

            // check hard disk space
            var backgroundThread2 = new Thread(() => {
                CheckSpaceAction();
            });
            backgroundThread2.Start();

            // check memory
            var backgroundThread3 = new Thread(() => {
                CheckMemoryAction();
            });
            backgroundThread3.Start();

            // check power
            var backgroundThread4 = new Thread(() => {
                //CheckPowerAction();
                WatchForPowerChanges();
            });
            backgroundThread4.Start();

            // check network
            var backgroundThread5 = new Thread(() => {
                //CheckNetworkAction();
                WatchForNetworkChanges();
            });
            backgroundThread5.Start();

            /*
            // check bitlocker
            var backgroundThread6 = new Thread(() => {
                //CheckBitlockerProtectionAction();
                WatchForBitLockerChanges();
            });
            backgroundThread6.Start();
            */

            // check hardware checks
            var ts7 = new ThreadStart(HardwareChecksPassedAction);
            var backgroundThread7 = new Thread(ts7);
            backgroundThread7.Start();

            /*
            // garbage collection bitlocker
            var gcBackgroundThread = new Thread(() => {
                Thread.Sleep(10000);
                GC.Collect();
            });
            gcBackgroundThread.Start();
            */
            // if success            
            //ButtonLeft2.IsEnabled = true;
            //ButtonRight2.IsEnabled = true;
        }

        private void GoToRemediateSpaceTab()
        {
            ShowRemediateSpaceTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Hidden;
            ButtonLeft1.Visibility = Visibility.Hidden;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Visible;
            ButtonLeft3.Visibility = Visibility.Visible;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;

            var ts10 = new ThreadStart(CleanupDiskSpace);
            var backgroundThread10 = new Thread(ts10);
            backgroundThread10.Start();
        }

        private void GoToInsufficientSpaceTab()
        {
            ShowInsufficientSpaceTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Hidden;
            ButtonLeft1.Visibility = Visibility.Hidden;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Visible;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;

            var ts20 = new ThreadStart(DiskCleanupFailed);
            var backgroundThread20 = new Thread(ts20);
            backgroundThread20.Start();
        }

        private void GoToInsufficientMemoryTab()
        {
            ShowInsufficientMemoryTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Hidden;
            ButtonLeft1.Visibility = Visibility.Hidden;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Visible;

            var ts30 = new ThreadStart(MemoryCheckFailed);
            var backgroundThread30 = new Thread(ts30);
            backgroundThread30.Start();
        }

        private void CloseApp_Click(object sender, RoutedEventArgs e)
        {
            ExitApp();
        }

        // Checks Tab button events
        private void GoToIntroTab_Click(object sender, RoutedEventArgs e)
        {
            ShowIntroTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Visible;
            ButtonLeft1.Visibility = Visibility.Visible;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Hidden;
            ButtonRight4.Visibility = Visibility.Hidden;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;
        }

        private void GoToReadyToUpdateTab_Click(object sender, RoutedEventArgs e)
        {
            ShowReadyToUpdateTab();

            // Tab 1 Buttons
            ButtonRight1.Visibility = Visibility.Hidden;
            ButtonLeft1.Visibility = Visibility.Hidden;

            // Tab 2 Buttons
            ButtonRight2.Visibility = Visibility.Hidden;
            ButtonLeft2.Visibility = Visibility.Hidden;
            ButtonRight7.Visibility = Visibility.Hidden;

            // Tab 3 Buttons
            ButtonRight3.Visibility = Visibility.Hidden;
            ButtonLeft3.Visibility = Visibility.Hidden;

            // Tab 4 Buttons
            ButtonLeft4.Visibility = Visibility.Visible;
            ButtonRight4.Visibility = Visibility.Visible;

            // Tab 5 Buttons
            ButtonRight5.Visibility = Visibility.Hidden;

            // Tab 6 Buttons
            ButtonRight6.Visibility = Visibility.Hidden;
        }
        // END Button Event definitions


        // BEGIN Tab Navigation controls
        private void ShowIntroTab()
        {
            logger.Info($"Showing Intro Tab");

            TabControl1.SelectedIndex = 0;
            currentTabIndex = 0;
        }

        private void ShowChecksTab()
        {
            logger.Info($"Showing Checks Tab");

            TabControl1.SelectedIndex = 1;
            currentTabIndex = 1;
        }

        private void ShowRemediateSpaceTab()
        {
            logger.Info($"Showing Remediate Space Tab");

            TabControl1.SelectedIndex = 2;
            currentTabIndex = 2;
        }

        private void ShowInsufficientSpaceTab()
        {
            logger.Info($"Showing Insufficient Space Tab");

            TabControl1.SelectedIndex = 4;
            currentTabIndex = 4;
        }

        private void ShowInsufficientMemoryTab()
        {
            logger.Info($"Showing Insufficient Memory Tab");

            TabControl1.SelectedIndex = 5;
            currentTabIndex = 5;
        }

        private void ShowReadyToUpdateTab()
        {
            logger.Info($"Showing Ready To Update Tab");
            
            TabControl1.SelectedIndex = 3;
            currentTabIndex = 3;
        }
        // END Tab Navigation controls

        // BEGIN Hardware Validation Action definitions
        private void CheckSpaceAction()
        {
            int currentAttempt = 1;
            int attemptLimit = 5;
            ulong hardDiskFreeSpaceRequirementGB = ulong.Parse(settings.GetSettingsItemValue("Hardware", "HardDiskFreeSpaceRequirementGB"));

            volumes = dataGatherer.GetVolumes();

            while ((volumes.Count() < 1) && (currentAttempt <= attemptLimit))
            {
                logger.Warn($"Volumes list returned with no entries. Waiting 2 seconds and trying to get the volumes list again. Attempt {currentAttempt} of {attemptLimit}");
                Thread.Sleep(2000);
                volumes = dataGatherer.GetVolumes();
                currentAttempt++;
            }

            if (volumes.Count() > 0)
            {
                spaceCheckPassed = hardwareValidator.CheckSpace(volumes, hardDiskFreeSpaceRequirementGB);
                if (spaceCheckPassed)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar1.Visibility = Visibility.Hidden;
                        LabelPassed1.Visibility = Visibility.Visible;
                        GreenTick1.Visibility = Visibility.Visible;
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar1.Visibility = Visibility.Hidden;
                        LabelPassed1.Visibility = Visibility.Visible;
                        LabelPassed1.Content = "Not enough space";
                        LabelPassed1.HorizontalAlignment = HorizontalAlignment.Center;
                        Redx1.Visibility = Visibility.Visible;

                        ButtonRight3.Opacity = 0.5;

                        //GoToRemediateSpaceTab();
                        GoToInsufficientSpaceTab();
                    });
                }
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.MetroWindow_Notice($"Error retrieving volume information. If this is the first time you are receiving this error, please close the application and try again.", $"Otherwise, please call the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}");
                    errorHandler.CopyLogs("VolumeCountError");
                });
            }

        }

        private void CheckMemoryAction()
        {
            ulong minimumMemoryRequirementGB = ulong.Parse(settings.GetSettingsItemValue("Hardware", "RAMRequiredGB"));
            memoryCheckPassed = hardwareValidator.CheckMemory(minimumMemoryRequirementGB);
            if (memoryCheckPassed)
            {
                // if success...
                this.Dispatcher.Invoke(() =>
                {
                    ProgressBar2.Visibility = Visibility.Hidden;
                    LabelPassed2.Visibility = Visibility.Visible;
                    GreenTick2.Visibility = Visibility.Visible;
                });
            }
            else
            {
                // else
                this.Dispatcher.Invoke(() =>
                {
                    ProgressBar2.Visibility = Visibility.Hidden;
                    LabelPassed2.Content = "Not enough RAM";
                    LabelPassed2.Visibility = Visibility.Visible;
                    LabelPassed2.HorizontalAlignment = HorizontalAlignment.Center;
                    Redx2.Visibility = Visibility.Visible;

                    GoToInsufficientMemoryTab();
                });
            }
        }

        private void CheckPowerAction()
        {
            //while ((currentTabIndex == 1) || !(this.powerCheckPassed))
            //{
            batteries = dataGatherer.GetBatteries();
            int powerCheckStatus = hardwareValidator.CheckPower(batteries);
            switch (powerCheckStatus)
            {
                case 0: // Not a laptop. No need to check power.
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar3.Visibility = Visibility.Hidden;
                        LabelPassed3.Visibility = Visibility.Visible;
                        ProgressRing1.Visibility = Visibility.Hidden;
                        LabelPassed3.Content = "Success";
                        GreenTick4.Visibility = Visibility.Visible;
                        Redx5.Visibility = Visibility.Hidden;
                        YellowWarn5.Visibility = Visibility.Hidden;
                    });
                    break;
                case 1: // Laptop, plugged in...
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar3.Visibility = Visibility.Hidden;
                        LabelPassed3.Visibility = Visibility.Visible;
                        ProgressRing1.Visibility = Visibility.Hidden;
                        LabelPassed3.Content = "Success";
                        GreenTick4.Visibility = Visibility.Visible;
                        Redx5.Visibility = Visibility.Hidden;
                        YellowWarn5.Visibility = Visibility.Hidden;
                    });
                    break;

                case 2: // Laptop, unplugged...
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar3.Visibility = Visibility.Hidden;
                        LabelPassed3.Visibility = Visibility.Visible;
                        ProgressRing1.Visibility = Visibility.Visible;
                        LabelPassed3.Content = "Connect to power";
                        GreenTick4.Visibility = Visibility.Hidden;
                        Redx5.Visibility = Visibility.Hidden;
                        YellowWarn5.Visibility = Visibility.Hidden;
                    });
                    break;

                case 3: // Laptop, no battery...
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar3.Visibility = Visibility.Hidden;
                        LabelPassed3.Visibility = Visibility.Visible;
                        ProgressRing1.Visibility = Visibility.Hidden;
                        LabelPassed3.Content = "Can't find battery";
                        GreenTick4.Visibility = Visibility.Hidden;
                        Redx5.Visibility = Visibility.Hidden;
                        YellowWarn5.Visibility = Visibility.Visible;
                    });
                    break;
            }
            Thread.Sleep(500);
            //}
        }

        private void WatchForPowerChanges()
        {
            CheckPowerAction();

            dataWatcher.StartBatteryCreationWatcher();
            dataWatcher.StartBatteryDeletionWatcher();
            dataWatcher.StartBatteryModificationWatcher();

            List<EventArrivedEventArgs> batteryCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> batteryDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> batteryModificationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldBatteryCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldBatteryDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldBatteryModificationEventList = new List<EventArrivedEventArgs>();

            while (currentTabIndex == 1)
            {
                batteryCreationEventList = dataWatcher.getBatteryCreationEventList();
                batteryDeletionEventList = dataWatcher.getBatteryDeletionEventList();
                batteryModificationEventList = dataWatcher.getBatteryModificationEventList();

                // Check if Batteries have been added, deleted, or modified
                if ((batteryCreationEventList.Count() > oldBatteryCreationEventList.Count())
                    || (batteryDeletionEventList.Count() > oldBatteryDeletionEventList.Count())
                    || (batteryModificationEventList.Count() > oldBatteryModificationEventList.Count()))
                {
                    // Test for Power 
                    CheckPowerAction();
                    oldBatteryCreationEventList = new List<EventArrivedEventArgs>(batteryCreationEventList);
                    oldBatteryDeletionEventList = new List<EventArrivedEventArgs>(batteryDeletionEventList);
                    oldBatteryModificationEventList = new List<EventArrivedEventArgs>(batteryModificationEventList);
                }

                Thread.Sleep(500);
            }

            dataWatcher.StopBatteryCreationWatcher();
            dataWatcher.StopBatteryDeletionWatcher();
            dataWatcher.StopBatteryModificationWatcher();
        }


        private void CheckNetworkAction()
        {
            string resultString;
            logger.Info($"Starting Network Check");
            logger.Info($"networkCheckPassed status: {this.networkCheckPassed}");

            netAdapters = dataGatherer.GetNetAdapters();
            int networkCheckStatus = hardwareValidator.CheckNetwork(netAdapters);
            switch (networkCheckStatus)
            {
                case 0:
                    // Domain Ethernet Check Passed
                    resultString = "Success";
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar5.Visibility = Visibility.Hidden;
                        LabelPassed5.Content = resultString;
                        ProgressRing10.Visibility = Visibility.Hidden;
                        LabelPassed5.Visibility = Visibility.Visible;
                        LabelPassed5.HorizontalAlignment = HorizontalAlignment.Center;
                        GreenTick6.Visibility = Visibility.Visible;
                        Redx7.Visibility = Visibility.Hidden;
                    });
                    break;
                case 1:
                    // No Domain Ethernet Found
                    resultString = "No domain ethernet found";
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar5.Visibility = Visibility.Hidden;
                        LabelPassed5.Content = resultString;
                        ProgressRing10.Visibility = Visibility.Hidden;
                        LabelPassed5.Visibility = Visibility.Visible;
                        LabelPassed5.HorizontalAlignment = HorizontalAlignment.Center;
                        GreenTick6.Visibility = Visibility.Hidden;
                        Redx7.Visibility = Visibility.Visible;
                    });
                    break;
                case 2:
                    // No Ethernet Found
                    resultString = "Connect ethernet cable";
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar5.Visibility = Visibility.Hidden;
                        LabelPassed5.Content = resultString;
                        ProgressRing10.Visibility = Visibility.Visible;
                        LabelPassed5.Visibility = Visibility.Visible;
                        LabelPassed5.HorizontalAlignment = HorizontalAlignment.Center;
                        GreenTick6.Visibility = Visibility.Hidden;
                        Redx7.Visibility = Visibility.Hidden;
                    });
                    break;
                case 3:
                    // No Network Connections
                    resultString = "No network connections";
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar5.Visibility = Visibility.Hidden;
                        LabelPassed5.Content = resultString;
                        ProgressRing10.Visibility = Visibility.Hidden;
                        LabelPassed5.Visibility = Visibility.Visible;
                        LabelPassed5.HorizontalAlignment = HorizontalAlignment.Center;
                        GreenTick6.Visibility = Visibility.Hidden;
                        Redx7.Visibility = Visibility.Visible;
                    });
                    break;
            }
            Thread.Sleep(500);
        }

        private void WatchForNetworkChanges()
        {
            CheckNetworkAction();

            dataWatcher.StartNetAdapterCreationWatcher();
            dataWatcher.StartNetAdapterDeletionWatcher();
            dataWatcher.StartNetAdapterModificationWatcher();

            List<EventArrivedEventArgs> netAdapterCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> netAdapterDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> netAdapterModificationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldNetAdapterCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldNetAdapterDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldNetAdapterModificationEventList = new List<EventArrivedEventArgs>();

            while (currentTabIndex == 1)
            {
                netAdapterCreationEventList = dataWatcher.getNetAdapterCreationEventList();
                netAdapterDeletionEventList = dataWatcher.getNetAdapterDeletionEventList();
                netAdapterModificationEventList = dataWatcher.getNetAdapterModificationEventList();

                // Check if NetAdapters have been added, deleted, or modified
                if ((netAdapterCreationEventList.Count() > oldNetAdapterCreationEventList.Count())
                    || (netAdapterDeletionEventList.Count() > oldNetAdapterDeletionEventList.Count())
                    || (netAdapterModificationEventList.Count() > oldNetAdapterModificationEventList.Count()))
                {
                    // Test for Network Connectivty device
                    CheckNetworkAction();
                    oldNetAdapterCreationEventList = new List<EventArrivedEventArgs>(netAdapterCreationEventList);
                    oldNetAdapterDeletionEventList = new List<EventArrivedEventArgs>(netAdapterDeletionEventList);
                    oldNetAdapterModificationEventList = new List<EventArrivedEventArgs>(netAdapterModificationEventList);
                }

                Thread.Sleep(500);
            }

            dataWatcher.StopNetAdapterCreationWatcher();
            dataWatcher.StopNetAdapterDeletionWatcher();
            dataWatcher.StopNetAdapterModificationWatcher();
        }

        /*
        private void CheckUSBAttachedAction()
        {
            while ((currentTabIndex == 1) && !(this.usbAttachedCheckPassed))
            {
                disks = dataGatherer.GetDisks();
                this.usbAttachedCheckPassed = hardwareValidator.CheckUSBAttached(disks);
                if (this.usbAttachedCheckPassed)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar8.Visibility = Visibility.Hidden;
                        ProgressRing8.Visibility = Visibility.Hidden;
                        LabelPassed8.Visibility = Visibility.Visible;
                        LabelPassed8.Content = "Success";
                        GreenTick8.Visibility = Visibility.Visible;
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar8.Visibility = Visibility.Hidden;
                        LabelPassed8.Visibility = Visibility.Visible;
                        ProgressRing8.Visibility = Visibility.Visible;
                        //Redx8.Visibility = Visibility.Visible;
                        GreenTick8.Visibility = Visibility.Hidden;
                        LabelPassed8.Content = "Remove USB storage";
                    });
                }
                Thread.Sleep(500);
            }
        }
        */

        private void CheckUsbAttachedAction()
        {
            int currentAttempt = 1;
            int attemptLimit = 5;

            disks = dataGatherer.GetDisks();
            while ((disks.Count() < 1) && (currentAttempt <= attemptLimit))
            {
                logger.Warn($"Disks list returned with no entries. Waiting 2 seconds and trying to get the disks list again. Attempt {currentAttempt} of {attemptLimit}");
                Thread.Sleep(2000);
                disks = dataGatherer.GetDisks();
                currentAttempt++;
            }

            if (disks.Count() > 0)
            {
                //this.usbAttachedCheckPassed = hardwareValidator.CheckUsbAttached(disks);
                int usbAttachedCheckStatus = hardwareValidator.CheckUsbAttached(disks);
                if (usbAttachedCheckStatus == 0)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar8.Visibility = Visibility.Hidden;
                        ProgressRing8.Visibility = Visibility.Hidden;
                        LabelPassed8.Visibility = Visibility.Visible;
                        LabelPassed8.Content = "Success";
                        GreenTick8.Visibility = Visibility.Visible;
                    });
                }
                else if (usbAttachedCheckStatus >= 1)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar8.Visibility = Visibility.Hidden;
                        LabelPassed8.Visibility = Visibility.Visible;
                        ProgressRing8.Visibility = Visibility.Visible;
                        //Redx8.Visibility = Visibility.Visible;
                        GreenTick8.Visibility = Visibility.Hidden;
                        LabelPassed8.Content = "Remove USB storage";
                    });
                }
                else // This would trigger if usbAttachedCheckStatus returns -1...
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        ProgressBar8.Visibility = Visibility.Hidden;
                        LabelPassed8.Visibility = Visibility.Visible;
                        // ProgressRing8.Visibility = Visibility.Visible;
                        Redx8.Visibility = Visibility.Visible;
                        // GreenTick8.Visibility = Visibility.Hidden;
                        LabelPassed8.Content = "Error";

                        this.MetroWindow_Notice($"Error retrieving USB drive information.", $"Please call the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}");
                        errorHandler.CopyLogs("USBDriveError");
                    });
                }
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.MetroWindow_Notice($"Error retrieving disk information. If this is the first time you are receiving this error, please close the application and try again.", $"Otherwise, please call the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}");
                    errorHandler.CopyLogs("DiskCountError");
                });
            }
        }

        private void WatchForUsbChanges()
        {
            CheckUsbAttachedAction();

            dataWatcher.StartDiskCreationWatcher();
            dataWatcher.StartDiskDeletionWatcher();

            List<EventArrivedEventArgs> diskCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> diskDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldDiskCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldDiskDeletionEventList = new List<EventArrivedEventArgs>();

            while (currentTabIndex == 1)
            {
                diskCreationEventList = dataWatcher.getDiskCreationEventList();
                diskDeletionEventList = dataWatcher.getDiskDeletionEventList();

                // Check if disks have been added or deleted
                if ((diskCreationEventList.Count() > oldDiskCreationEventList.Count()) || (diskDeletionEventList.Count() > oldDiskDeletionEventList.Count()))
                {
                    // Test for USB device
                    CheckUsbAttachedAction();
                    oldDiskCreationEventList = new List<EventArrivedEventArgs>(diskCreationEventList);
                    oldDiskDeletionEventList = new List<EventArrivedEventArgs>(diskDeletionEventList);
                }

                Thread.Sleep(500);
            }

            dataWatcher.StopDiskCreationWatcher();
            dataWatcher.StopDiskDeletionWatcher();
        }

        /*
        // Act on Bitlocker Status result
        private void CheckBitlockerProtectionAction()
        {

                encryptableVolumes = dataGatherer.GetEncryptableVolumes();
                var osEncryptableVolumeQuery = from encryptableVolume in encryptableVolumes
                                               where encryptableVolume.DriveLetter == $"{computerInfo.SystemDrive}"
                                               select encryptableVolume;

                if (osEncryptableVolumeQuery.Count() > 0)
                {
                    string resultString;

                    foreach (EncryptableVolume osEncryptableVolume in osEncryptableVolumeQuery)
                    {
                        int bitlockerCheckStatus = hardwareValidator.CheckBitlockerProtection(osEncryptableVolume);

                        switch (bitlockerCheckStatus)
                        {
                            case 0: // volume is fully decrypted 
                                //this.bitlockerCheckPassed = true;
                                resultString = "Fully decrypted";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    YellowWarn9.Visibility = Visibility.Visible;
                                });
                                break;

                            case 1: // volume is fully encrypted
                                //this.bitlockerCheckPassed = true;
                                resultString = "Success";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    GreenTick9.Visibility = Visibility.Visible;
                                });
                                break;
                            case 2: // encryption in progress
                                resultString = "Encryption in progress";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    LabelPassed9.HorizontalAlignment = HorizontalAlignment.Center;
                                    Redx9.Visibility = Visibility.Visible;
                                });
                                break;
                            case 3: // decryption in progress
                                resultString = "Decryption in progress";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    LabelPassed9.HorizontalAlignment = HorizontalAlignment.Center;
                                    Redx9.Visibility = Visibility.Visible;
                                });
                                break;
                            case 4: // encryption paused
                                resultString = "Encryption paused";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    LabelPassed9.HorizontalAlignment = HorizontalAlignment.Center;
                                    Redx9.Visibility = Visibility.Visible;
                                });
                                break;
                            case 5: // decryption in progress
                                resultString = "Decryption paused";
                                this.Dispatcher.Invoke(() =>
                                {
                                    ProgressBar9.Visibility = Visibility.Hidden;
                                    LabelPassed9.Content = resultString;
                                    LabelPassed9.Visibility = Visibility.Visible;
                                    LabelPassed9.HorizontalAlignment = HorizontalAlignment.Center;
                                    Redx9.Visibility = Visibility.Visible;
                                });
                                break;
                        }
                    }
                }
                Thread.Sleep(500);

        }
        */

        /*
        private void WatchForBitLockerChanges()
        {
            CheckBitlockerProtectionAction();

            dataWatcher.StartEncryptableVolumeCreationWatcher();
            dataWatcher.StartEncryptableVolumeDeletionWatcher();
            dataWatcher.StartEncryptableVolumeModificationWatcher();

            List<EventArrivedEventArgs> encryptableVolumeCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> encryptableVolumeDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> encryptableVolumeModificationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldEncryptableVolumeCreationEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldEncryptableVolumeDeletionEventList = new List<EventArrivedEventArgs>();
            List<EventArrivedEventArgs> oldEncryptableVolumeModificationEventList = new List<EventArrivedEventArgs>();

            while (currentTabIndex == 1)
            {
                encryptableVolumeCreationEventList = dataWatcher.getEncryptableVolumeCreationEventList();
                encryptableVolumeDeletionEventList = dataWatcher.getEncryptableVolumeDeletionEventList();
                encryptableVolumeModificationEventList = dataWatcher.getEncryptableVolumeModificationEventList();

                // Check if encryptable volumes have been added, deleted or modified
                if ((encryptableVolumeCreationEventList.Count() > oldEncryptableVolumeCreationEventList.Count()) 
                    || (encryptableVolumeDeletionEventList.Count() > oldEncryptableVolumeDeletionEventList.Count())
                    || (encryptableVolumeModificationEventList.Count() > oldEncryptableVolumeModificationEventList.Count()))
                {
                    // Test for Bitlocker Protection
                    CheckBitlockerProtectionAction();
                    oldEncryptableVolumeCreationEventList = new List<EventArrivedEventArgs>(encryptableVolumeCreationEventList);
                    oldEncryptableVolumeDeletionEventList = new List<EventArrivedEventArgs>(encryptableVolumeDeletionEventList);
                    oldEncryptableVolumeModificationEventList = new List<EventArrivedEventArgs>(encryptableVolumeModificationEventList);

                }

                Thread.Sleep(500);
            }

            dataWatcher.StopEncryptableVolumeCreationWatcher();
            dataWatcher.StopEncryptableVolumeDeletionWatcher();
            dataWatcher.StopEncryptableVolumeModificationWatcher();
        }
        */

        // Act on Hardware checks passing or failing
        private void HardwareChecksPassedAction()
        {
            while (currentTabIndex == 1)
            {
                int hardwareCheckStatus = hardwareValidator.HardwareChecksPassed();
                switch (hardwareCheckStatus)
                {
                    case 0: // Fail if Memory too low
                        this.Dispatcher.Invoke(() =>
                        {
                            //ButtonLeft2.IsEnabled = true;
                            ButtonRight2.Opacity = 1;
                            ButtonRight2.IsEnabled = true;
                            ButtonRight2.Click -= GoToReadyToUpdateTab_Click;
                            ButtonRight2.Click += CloseApp_Click;
                            ButtonRight2.Content = "Close";
                        });
                        break;
                    case 1: // Pass if all passed
                        //this.hardwareChecksPassed = true;
                        this.Dispatcher.Invoke(() =>
                        {
                            //ButtonLeft2.IsEnabled = true;
                            ButtonRight2.Opacity = 1;
                            ButtonRight2.IsEnabled = true;
                            if ((string)ButtonRight2.Content == "Close")
                            {
                                ButtonRight2.Click -= CloseApp_Click;
                                ButtonRight2.Click += GoToReadyToUpdateTab_Click;
                                ButtonRight2.Content = "Next";
                            }

                        });
                        break;
                    case 2: // Non-critical failure
                        //this.hardwareChecksPassed = true;
                        this.Dispatcher.Invoke(() =>
                        {
                            //ButtonLeft2.IsEnabled = true;
                            ButtonRight2.Opacity = 0.5;
                            ButtonRight2.IsEnabled = false;
                            if ((string)ButtonRight2.Content == "Close")
                            {
                                ButtonRight2.Click -= CloseApp_Click;
                                ButtonRight2.Click += GoToReadyToUpdateTab_Click;
                                ButtonRight2.Content = "Next";
                            }
                        });
                        break;
                }
                Thread.Sleep(500);
            }
        }
        // END Hardware Validation Action definitions

        private void CleanupDiskSpace()
        {
            DiskCleanup diskCleanup = new DiskCleanup();
            Thread.Sleep(5000);

            bool spaceRemediationResult = false;

            if (spaceRemediationResult)
            {
                this.Dispatcher.Invoke(() =>
                {
                    spaceCheckPassed = true;
                    Label5.Visibility = Visibility.Hidden;
                    Label6.Visibility = Visibility.Visible;
                    ProgressRing.Visibility = Visibility.Hidden;
                    GreenTick3.Visibility = Visibility.Visible;
                    ButtonRight3.IsEnabled = true;
                    ButtonRight3.Opacity = 1;
                });
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    spaceCheckPassed = false;
                    Label5.Visibility = Visibility.Visible;
                    Label6.Visibility = Visibility.Hidden;
                    ProgressRing.Visibility = Visibility.Hidden;
                    GreenTick3.Visibility = Visibility.Hidden;
                    ButtonRight3.IsEnabled = false;
                    ButtonRight3.Opacity = 0.5;

                    GoToInsufficientSpaceTab();
                });
            }
        }

        private void DiskCleanupFailed()
        {
            ulong currentFreeSpaceBytes = dataGatherer.GetOsVolumeFreeSpaceBytes();
            ulong currentFreeSpaceGB = currentFreeSpaceBytes / 1024 / 1024 / 1024;

            this.Dispatcher.Invoke(() =>
            {
                TextBlock6.Text = $"{settings.GetSettingsItemValue("Hardware", "HardDiskFreeSpaceRequirementGB")} GB free space required";
                TextBlock7.Text = $"Only {currentFreeSpaceGB} GB free space on this machine";
            });
        }

        private void MemoryCheckFailed()
        {
            ulong currentTotalPhysicalMemory = computerInfo.TotalPhysicalMemory;
            ulong currentTotalPhysicalMemoryGB = currentTotalPhysicalMemory / 1024 / 1024 / 1024;

            this.Dispatcher.Invoke(() =>
            {
                TextBlock9.Text = $"{settings.GetSettingsItemValue("Hardware", "RAMRequiredGB")} GB RAM required";
                TextBlock10.Text = $"Only {currentTotalPhysicalMemoryGB} GB RAM on this machine";
            });
        }

        private void InstallUPGBackground(string upgBackgroundValidatedDirectoryPath)
        {
            logger.Info("Start install of UPGBackground");
            DirectoryInfo upgBackgroundDirectoryInfo = new DirectoryInfo(upgBackgroundValidatedDirectoryPath);

            if (upgBackgroundDirectoryInfo.Exists)
            {
                var fileQuery = from file in upgBackgroundDirectoryInfo.GetFiles()
                                where file.Name.Contains("UPGBackground")
                                where file.Extension == ".msi"
                                select file;

                if (fileQuery.Count() == 1)
                {
                    foreach (var file in fileQuery)
                    {
                        // Install UPGBackground
                        string installCommand = $"msiexec.exe /i\"{file.FullName}\" /qn";
                        logger.Info($"Running install command: {installCommand}.");
                        App.ExecuteCommand(installCommand);
                    }
                }
                else
                {
                    logger.Error($"An error occured trying to install UPGBackground");
                }
            }
            else
            {
                logger.Error($"UPGBackground path '{upgBackgroundValidatedDirectoryPath}' does not exist.");
            }
        }

        private void ValidateSettings()
        {
            SettingsItemPathValidationResult settingsItemPathValidationResult;
            string contactHelpDesk = $"Please contact the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}.";

            // Validate LogSharePath
            if (!(this.settingsValidator.ValidateLogSharePath()))
            {
                // LogSharePath Validation failed
                logger.Error($"LogSharePath validation failed.");
                this.MetroWindow_Notice($"LogSharePath location not found or inaccessible...", contactHelpDesk);
            }

            // Validate RunTSXmlPath
            settingsItemPathValidationResult = this.settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "RunTSXmlPath"), true);
            if (settingsItemPathValidationResult.Result)
            {
                // RunTSXmlPath Validated
                logger.Info($"{settingsItemPathValidationResult.SettingsItem.ItemName} resolved to '{settingsItemPathValidationResult.ValidatedPath}'");

                // Validate Scheduled Task Run TS XML Content
                logger.Info($"Validating Scheduled Task Run TS XML content.");
                string scheduledTaskRunTSXmlStatus = this.settingsValidator.ValidateScheduledTaskXml(settingsItemPathValidationResult.ValidatedPath);
                logger.Info($"Scheduled Task Run TS XML content status: {scheduledTaskRunTSXmlStatus}");
                switch (scheduledTaskRunTSXmlStatus)
                {
                    case "Valid":
                        logger.Info($"Scheduled Task Run TS XML content is valid.");
                        break;
                    case "Working Directory not found":
                        logger.Error($"Scheduled Task Run TS XML content has an invalid working directory.");
                        this.MetroWindow_Notice($"Scheduled Task Run TS XML content has an invalid working directory...", contactHelpDesk);
                        break;
                    case "Command file path not found":
                        logger.Error($"Scheduled Task Run TS XML content has an invalid command file path.");
                        this.MetroWindow_Notice($"Scheduled Task Run TS XML content has an invalid command file path...", contactHelpDesk);
                        break;
                }
            }
            else
            {
                // RunTSXmlPath Validation failed
                logger.Error($"Unable to find {settingsItemPathValidationResult.SettingsItem.ItemName} at specified path: '{settingsItemPathValidationResult.SettingsItem.ItemValue}'");
                this.MetroWindow_Notice($"{settingsItemPathValidationResult.SettingsItem.ItemName} not found...", contactHelpDesk);
            }



            // Validate CleanupXmlPath
            settingsItemPathValidationResult = this.settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "CleanupXmlPath"), true);
            if (settingsItemPathValidationResult.Result)
            {
                // CleanupXmlPath Validated
                logger.Info($"{settingsItemPathValidationResult.SettingsItem.ItemName} resolved to '{settingsItemPathValidationResult.ValidatedPath}'");

                // Validate ScheduledTaskCleanupOnTSStartFailureXml Content
                logger.Info($"Validating ScheduledTaskCleanupOnTSStartFailureXml content.");
                string scheduledTaskCleanupOnTSStartFailureXmlStatus = this.settingsValidator.ValidateScheduledTaskXml(settingsItemPathValidationResult.ValidatedPath);
                logger.Info($"ScheduledTaskCleanupOnTSStartFailureXml content status: {scheduledTaskCleanupOnTSStartFailureXmlStatus}");
                switch (scheduledTaskCleanupOnTSStartFailureXmlStatus)
                {
                    case "Valid":
                        logger.Info($"ScheduledTaskCleanupOnTSStartFailureXml content is valid.");
                        break;
                    case "Working Directory not found":
                        logger.Error($"ScheduledTaskCleanupOnTSStartFailureXml content has an invalid working directory.");
                        this.MetroWindow_Notice($"ScheduledTaskCleanupOnTSStartFailureXml content has an invalid working directory...", contactHelpDesk);
                        break;
                    case "Command file path not found":
                        logger.Error($"ScheduledTaskCleanupOnTSStartFailureXml content has an invalid command file path.");
                        this.MetroWindow_Notice($"ScheduledTaskCleanupOnTSStartFailureXml content has an invalid command file path...", contactHelpDesk);
                        break;
                }
            }
            else
            {
                // CleanupXmlPath Validation failed
                logger.Error($"Unable to find {settingsItemPathValidationResult.SettingsItem.ItemName} at specified path: '{settingsItemPathValidationResult.SettingsItem.ItemValue}'");
                this.MetroWindow_Notice($"{settingsItemPathValidationResult.SettingsItem.ItemName} not found...", contactHelpDesk);
            }



            // Validate UPGBackgroundDirectoryPath
            settingsItemPathValidationResult = this.settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("PostGUI", "UPGBackgroundDirectoryPath"), true);
            if (settingsItemPathValidationResult.Result)
            {
                // UPGBackgroundDirectoryPath Validated
                logger.Info($"{settingsItemPathValidationResult.SettingsItem.ItemName} resolved to '{settingsItemPathValidationResult.ValidatedPath}'");

            }
            else
            {
                // UPGBackgroundDirectoryPath Validation failed
                logger.Error($"Unable to find {settingsItemPathValidationResult.SettingsItem.ItemName} at specified path: '{settingsItemPathValidationResult.SettingsItem.ItemValue}'");
                this.MetroWindow_Notice($"{settingsItemPathValidationResult.SettingsItem.ItemName} not found...", contactHelpDesk);
            }
        }


        //private bool _shutdown;
        //private readonly MainWindowViewModel _viewModel;

        /*
        private async void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !_shutdown;
            if (_shutdown) return;

            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Quit",
                NegativeButtonText = "Cancel",
                AnimateShow = true,
                AnimateHide = false
            };

            var result = await this.ShowMessageAsync("Quit application?",
                "Sure you want to quit application?",
                MessageDialogStyle.AffirmativeAndNegative, mySettings);

            _shutdown = result == MessageDialogResult.Affirmative;

            if (_shutdown)
                Application.Current.Shutdown();
        }
        */

        // BEGIN Start Button Event definitions
        private void StartUpdate_Click(object sender, RoutedEventArgs e)
        {
            logger.Info($"Start Update button has been clicked.");

            StartCountdown();    // Start the countdown

            // Show the countdown
            RestartBox.Visibility = Visibility.Visible;

            // Change the Start Update button text and click action to Cancel
            ButtonRight4.Click -= StartUpdate_Click;
            ButtonRight4.Click += CancelCountdown_Click;
            ButtonRight4.Content = "Cancel";

            // Change the Back button text and click action to Restart Now
            ButtonLeft4.Click -= GoToChecksTab_Click;
            ButtonLeft4.Click += RestartNow_Click;
            ButtonLeft4.Content = "Restart Now";
        }

        private void CancelCountdown_Click(object sender, RoutedEventArgs e)
        {
            logger.Info($"Cancel Countdown button has been clicked.");

            countdownTimer.Stop();  // Stop the countdown

            // Revert back to original Start Update button state
            ButtonRight4.Content = "Start Update";
            RestartBox.Visibility = Visibility.Hidden;
            ButtonRight4.Click -= CancelCountdown_Click;
            ButtonRight4.Click += StartUpdate_Click;

            // Revert the Back button to original state
            ButtonLeft4.Click -= RestartNow_Click;
            ButtonLeft4.Click += GoToChecksTab_Click;
            ButtonLeft4.Content = "Back";
        }

        private void StartCountdown()
        {
            restartMinutes = int.Parse(settings.GetSettingsItemValue("Other", "RestartCountdownMin"));
            restartTimeSpan = TimeSpan.FromMinutes(restartMinutes);
            restartSeconds = (int)restartTimeSpan.TotalSeconds;

            logger.Warn($"Countdown started. Computer will execute Post-GUI actions and then restart in {restartSeconds} seconds.");
            RestartBox.Text = $"Restarting in...";
            countdownTimer = new DispatcherTimer();
            countdownTimer.Interval = TimeSpan.FromSeconds(1);
            countdownTimer.Tick += CountdownTimer_Tick;
            countdownTimer.Start();
        }

        async void CountdownTimer_Tick(object sender, object e)
        {
            if (restartSeconds >= 0)
            {
                await this.Dispatcher.InvokeAsync(() =>
                {
                    RestartBox.Text = $"Restarting in {restartSeconds.ToString()} seconds";
                    RestartBox.Visibility = Visibility.Visible;
                }, DispatcherPriority.Input);
            }
            else
            {
                countdownTimer.Stop();
                this.Dispatcher.Invoke(() =>
                {
                    // Run Post-GUI steps and restart
                    logger.Info($"The countdown has reached 0. Executing Post-GUI actions and restarting computer.");
                    RestartBox.Text = "Restarting...";

                    // Register the Task Sequence Scheduled Task
                    scheduledTaskRunner.RegisterScheduledTask(null, runTSXmlPath);

                    // Set the Legal Notice
                    legalNoticeSetter.SetLegalNoticeRegKeys(legalNoticeCaption, legalNoticeText);

                    // Install UPGBackground
                    string upgBackgroundValidatedDirectoryPath = this.settingsValidator.ValidateSettingsItemPath(this.settings.GetSettingsItem("PostGUI", "UPGBackgroundDirectoryPath"), true).ValidatedPath;
                    InstallUPGBackground(upgBackgroundValidatedDirectoryPath);

                    // Run Post-GUI Action
                    string postGuiAction = this.settings.GetSettingsItemValue("PostGUI", "PostGUIAction");
                    string outputFile = $"{Environment.GetEnvironmentVariable("windir")}\\useremail";
                    App.ExecuteCommand(postGuiAction + $" {Process.GetCurrentProcess().Id} " + outputFile);

                    // Restart computer
                    logger.Warn("Restarting computer.");
                    App.RestartComputer();
                });
            }

            restartSeconds--;
        }
        // END Start Button Event definitions

        private void RestartNow_Click(object sender, RoutedEventArgs e)
        {
            logger.Info($"Restart Now button has been clicked...");

            countdownTimer.Stop();  // Stop the countdown

            // Run Post-GUI steps and restart
            logger.Info($"The countdown has reached 0. Executing Post-GUI actions and restarting computer.");
            RestartBox.Text = "Restarting...";

            // Register the Task Sequence Scheduled Task
            scheduledTaskRunner.RegisterScheduledTask(null, runTSXmlPath);

            // Set the Legal Notice
            legalNoticeSetter.SetLegalNoticeRegKeys(legalNoticeCaption, legalNoticeText);

            // Install UPGBackground
            string upgBackgroundValidatedDirectoryPath = this.settingsValidator.ValidateSettingsItemPath(this.settings.GetSettingsItem("PostGUI", "UPGBackgroundDirectoryPath"), true).ValidatedPath;
            InstallUPGBackground(upgBackgroundValidatedDirectoryPath);

            // Run Post-GUI Action
            string postGuiAction = this.settings.GetSettingsItemValue("PostGUI", "PostGUIAction");
            string outputFile = $"{Environment.GetEnvironmentVariable("windir")}\\useremail";
            App.ExecuteCommand(postGuiAction + $" {Process.GetCurrentProcess().Id} " + outputFile);

            // Restart computer
            logger.Warn("Restarting computer.");
            App.RestartComputer();
        }

        public async void MetroWindow_Notice(string message1, string message2)
        {
            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Exit",
                //NegativeButtonText = "Cancel",           
                AnimateShow = true,
                AnimateHide = false
            };

            var result = await this.ShowMessageAsync(message1, message2, MessageDialogStyle.Affirmative, mySettings);
            if (result == MessageDialogResult.Affirmative)
            {
                ExitApp();
            }
        }


        private void ExitApp()
        {
            dataWatcher.StopAllWatchers();
            Environment.Exit(Environment.ExitCode);
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ExitApp();
        }
    }
}
