﻿using Microsoft.Management.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class DataGatherer
    {
        public List<PhysicalDisk> PhysicalDisks { get; set; }
        public List<Volume> Volumes { get; set; }
        public List<Battery> Batteries { get; set; }
        public List<NetAdapter> NetAdapters { get; set; }
        public List<EncryptableVolume> EncryptableVolumes { get; set; }
        public static NLog.Logger logger;

        public DataGatherer()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        // START Data Gathering methods
        // Get Physical Disk info from MSFT_PhysicalDisk
        public List<PhysicalDisk> GetPhysicalDisks()
        {
            List<PhysicalDisk> physicalDisks = new List<PhysicalDisk>();

            string computer = "localhost";
            using (CimSession session = CimSession.Create(computer))
            {
                // Query Physical Disks
                var cimInstanceList = session.QueryInstances(@"root/microsoft/windows/storage", "WQL", "SELECT BusType, DeviceId, FriendlyName, MediaType, Model, ObjectId, PhysicalLocation, UniqueId FROM MSFT_PhysicalDisk");

                if (cimInstanceList != null)
                {
                    // Loop through all physical disks
                    foreach (CimInstance cimInstance in cimInstanceList)
                    {
                        PhysicalDisk physicalDisk = new PhysicalDisk
                        {
                            BusType = (ushort)cimInstance.CimInstanceProperties["BusType"].Value,
                            DeviceId = (string)cimInstance.CimInstanceProperties["DeviceId"].Value,
                            FriendlyName = (string)cimInstance.CimInstanceProperties["FriendlyName"].Value,
                            MediaType = (ushort)cimInstance.CimInstanceProperties["MediaType"].Value,
                            Model = (string)cimInstance.CimInstanceProperties["Model"].Value,
                            ObjectId = (string)cimInstance.CimInstanceProperties["ObjectId"].Value,
                            PhysicalLocation = (string)cimInstance.CimInstanceProperties["PhysicalLocation"].Value,
                            UniqueId = (string)cimInstance.CimInstanceProperties["UniqueId"].Value
                        };

                        physicalDisks.Add(physicalDisk);
                    }
                }
            }            

            return physicalDisks;
        }

        // Get Physical Disk info from MSFT_PhysicalDisk
        public List<Disk> GetDisks()
        {
            List<Disk> disks = new List<Disk>();

            string computer = "localhost";
            using (CimSession session = CimSession.Create(computer))
            {
                // Query Physical Disks
                var cimInstanceList = session.QueryInstances(@"root/microsoft/windows/storage", "WQL", "SELECT BusType, FriendlyName, IsBoot, IsOffline, IsReadOnly, IsSystem, Model, Number, ObjectId, OperationalStatus, PartitionStyle, UniqueId FROM MSFT_Disk");

                if (cimInstanceList != null)
                {
                    // Loop through all physical disks
                    foreach (CimInstance cimInstance in cimInstanceList)
                    {
                        Disk disk = new Disk
                        {
                            BusType = (ushort)cimInstance.CimInstanceProperties["BusType"].Value,
                            FriendlyName = (string)cimInstance.CimInstanceProperties["FriendlyName"].Value,
                            IsBoot = (bool)cimInstance.CimInstanceProperties["IsBoot"].Value,
                            IsOffline = (bool)cimInstance.CimInstanceProperties["IsOffline"].Value,
                            IsReadOnly = (bool)cimInstance.CimInstanceProperties["IsReadOnly"].Value,
                            IsSystem = (bool)cimInstance.CimInstanceProperties["IsSystem"].Value,
                            Model = (string)cimInstance.CimInstanceProperties["Model"].Value,
                            Number = (uint)cimInstance.CimInstanceProperties["Number"].Value,
                            ObjectId = (string)cimInstance.CimInstanceProperties["ObjectId"].Value,
                            OperationalStatus = (ushort[])cimInstance.CimInstanceProperties["OperationalStatus"].Value,
                            PartitionStyle = (ushort)cimInstance.CimInstanceProperties["PartitionStyle"].Value,
                            UniqueId = (string)cimInstance.CimInstanceProperties["UniqueId"].Value
                        };

                        disks.Add(disk);
                    }
                }
            }

            return disks;
        }

        // Get Volume info from MSFT_Volume
        public List<Volume> GetVolumes()
        {
            List<Volume> msftVolumeList = new List<Volume>();
            string computer = "localhost";
            using (CimSession session = CimSession.Create(computer))
            {
                // Query Volumes, returns CimInstances
                var cimInstanceList = session.QueryInstances(@"root/microsoft/windows/storage", "WQL", "SELECT * FROM MSFT_Volume");

                if (cimInstanceList != null)
                {
                    // Loop through all volumes
                    foreach (CimInstance cimInstance in cimInstanceList)
                    {
                        if (cimInstance.CimInstanceProperties["DriveLetter"].Value != null)
                        {
                            Volume msftVolume = new Volume
                            {
                                UniqueId = (string)cimInstance.CimInstanceProperties["UniqueId"].Value,
                                DriveLetter = (char)cimInstance.CimInstanceProperties["DriveLetter"].Value,
                                DriveType = (uint)cimInstance.CimInstanceProperties["DriveType"].Value,
                                FileSystem = (string)cimInstance.CimInstanceProperties["FileSystem"].Value,
                                FileSystemLabel = (string)cimInstance.CimInstanceProperties["FileSystemLabel"].Value,
                                FileSystemType = (ushort)cimInstance.CimInstanceProperties["FileSystemType"].Value,
                                Path = (string)cimInstance.CimInstanceProperties["Path"].Value,
                                Size = (ulong)cimInstance.CimInstanceProperties["Size"].Value,
                                SizeRemaining = (ulong)cimInstance.CimInstanceProperties["SizeRemaining"].Value
                            };

                            msftVolumeList.Add(msftVolume);
                        }
                    }
                }
            }

            return msftVolumeList;
        }

        // Get Battery info from Win32_Battery
        public List<Battery> GetBatteries()
        {
            List<Battery> batteryList = new List<Battery>();
            string computer = "localhost";
            using (CimSession session = CimSession.Create(computer))
            {
                // Query Win32_Battery
                var cimInstanceList = session.QueryInstances(@"root/cimv2", "WQL", "SELECT * FROM Win32_Battery");

                if (cimInstanceList != null)
                {
                    foreach (CimInstance cimInstance in cimInstanceList)
                    {
                        if (cimInstance.CimInstanceProperties["BatteryStatus"].Value != null)
                        {
                            Battery battery = new Battery()
                            {
                                Availability = (ushort)cimInstance.CimInstanceProperties["Availability"].Value,
                                BatteryStatus = (ushort)cimInstance.CimInstanceProperties["BatteryStatus"].Value,
                                Caption = (string)cimInstance.CimInstanceProperties["Caption"].Value,
                                // Chemistry = (ushort)cimInstance.CimInstanceProperties["Chemistry"].Value,
                                Description = (string)cimInstance.CimInstanceProperties["Description"].Value,
                                // DesignVoltage = (ulong)cimInstance.CimInstanceProperties["DesignVoltage"].Value,
                                DeviceID = (string)cimInstance.CimInstanceProperties["DeviceID"].Value,
                                // EstimatedChargeRemaining = (ushort)cimInstance.CimInstanceProperties["EstimatedChargeRemaining"].Value,
                                // EstimatedRunTime = (uint)cimInstance.CimInstanceProperties["EstimatedRunTime"].Value,
                                Name = (string)cimInstance.CimInstanceProperties["Name"].Value,
                                // PowerManagementCapabilities = (ushort[])cimInstance.CimInstanceProperties["PowerManagementCapabilities"].Value,
                                // PowerManagementSupported = (bool)cimInstance.CimInstanceProperties["PowerManagementSupported"].Value,
                                Status = (string)cimInstance.CimInstanceProperties["Status"].Value
                            };

                            batteryList.Add(battery);
                        }
                    }
                }
            }

            return batteryList;
        }

        // Get Network Adapter info from MSFT_NetAdapter and Win32_NetworkAdapterConfiguration
        public List<NetAdapter> GetNetAdapters()
        {
            List<NetAdapter> netAdapterList = new List<NetAdapter>();
            string computer = "localhost";
            using (CimSession session = CimSession.Create(computer))
            {
                // Query MSFT_NetAdapter
                var cimNetAdapterList = session.QueryInstances(@"root/StandardCimv2", "WQL", "SELECT ConnectorPresent, DeviceID, DeviceName, DriverDescription, DriverName, " +
                    "DriverProvider, DriverVersionString, EnabledState, HardwareInterface, InterfaceName, InterfaceDescription, InterfaceType, " +
                    "MediaConnectState, Name, NetworkAddresses, PermanentAddress, PnPDeviceID, State FROM MSFT_NetAdapter");

                // Query Win32_NetworkAdapterConfiguration
                var cimNetworkAdapterConfigurationList = session.QueryInstances(@"root/cimv2", "WQL", "SELECT MACAddress, IPAddress, DNSDomain, DHCPServer, DefaultIPGateway, IPSubnet FROM Win32_NetworkAdapterConfiguration WHERE MACAddress != null AND IpEnabled = True");

                if (cimNetAdapterList != null)
                {
                    // Loop through all net adapters
                    foreach (CimInstance cimNetAdapter in cimNetAdapterList)
                    {
                        NetAdapter netAdapter = new NetAdapter
                        {
                            ConnectorPresent = (bool)cimNetAdapter.CimInstanceProperties["ConnectorPresent"].Value,
                            DeviceID = (string)cimNetAdapter.CimInstanceProperties["DeviceID"].Value,
                            DeviceName = (string)cimNetAdapter.CimInstanceProperties["DeviceName"].Value,
                            DriverDescription = (string)cimNetAdapter.CimInstanceProperties["DriverDescription"].Value,
                            DriverName = (string)cimNetAdapter.CimInstanceProperties["DriverName"].Value,
                            DriverProvider = (string)cimNetAdapter.CimInstanceProperties["DriverProvider"].Value,
                            DriverVersionString = (string)cimNetAdapter.CimInstanceProperties["DriverVersionString"].Value,
                            EnabledState = (ushort)cimNetAdapter.CimInstanceProperties["EnabledState"].Value,
                            HardwareInterface = (bool)cimNetAdapter.CimInstanceProperties["HardwareInterface"].Value,
                            InterfaceName = (string)cimNetAdapter.CimInstanceProperties["InterfaceName"].Value,
                            InterfaceDescription = (string)cimNetAdapter.CimInstanceProperties["InterfaceDescription"].Value,
                            InterfaceType = (uint)cimNetAdapter.CimInstanceProperties["InterfaceType"].Value,
                            MediaConnectState = (uint)cimNetAdapter.CimInstanceProperties["MediaConnectState"].Value,
                            Name = (string)cimNetAdapter.CimInstanceProperties["Name"].Value,
                            NetworkAddresses = (string[])cimNetAdapter.CimInstanceProperties["NetworkAddresses"].Value,
                            PermanentAddress = (string)cimNetAdapter.CimInstanceProperties["PermanentAddress"].Value,
                            PnPDeviceID = (string)cimNetAdapter.CimInstanceProperties["PnPDeviceID"].Value,
                            State = (uint)cimNetAdapter.CimInstanceProperties["State"].Value
                        };

                        string unformattedMACAddress = (string)cimNetAdapter.CimInstanceProperties["PermanentAddress"].Value;
                        string formattedMACAddress = string.Join(":", Enumerable.Range(0, 6)
                            .Select(i => unformattedMACAddress.Substring(i * 2, 2)));

                        // match NetAdapter to NetworkAdapterConfiguration instance
                        var networkAdapterConfigurationQuery = from x in cimNetworkAdapterConfigurationList
                                                              where (string)x.CimInstanceProperties["MACAddress"].Value == formattedMACAddress
                                                              select x;


                        if (networkAdapterConfigurationQuery.Count() == 1)
                        {
                            foreach (CimInstance networkAdapterConfiguration in networkAdapterConfigurationQuery)
                            {
                                netAdapter.MACAddress = (string)networkAdapterConfiguration.CimInstanceProperties["MACAddress"].Value;
                                netAdapter.IPAddress = (string[])networkAdapterConfiguration.CimInstanceProperties["IPAddress"].Value;
                                netAdapter.DNSDomain = (string)networkAdapterConfiguration.CimInstanceProperties["DNSDomain"].Value;
                                netAdapter.DHCPServer = (string)networkAdapterConfiguration.CimInstanceProperties["DHCPServer"].Value;
                                netAdapter.DefaultIPGateway = (string[])networkAdapterConfiguration.CimInstanceProperties["DefaultIPGateway"].Value;
                                netAdapter.IPSubnet = (string[])networkAdapterConfiguration.CimInstanceProperties["IPSubnet"].Value;
                            }
                        }

                        netAdapterList.Add(netAdapter);
                    }
                }
            }

            return netAdapterList;
        }

        // Get info about volume encryption configuration from Win32_EncryptableVolume
        public List<EncryptableVolume> GetEncryptableVolumes()
        {
            List<EncryptableVolume> encryptableVolumes = new List<EncryptableVolume>();
            string computer = "localhost";
            try
            {
                using (CimSession session = CimSession.Create(computer))
                {
                    // Query Win32_EncryptableVolume
                    var cimInstances = session.QueryInstances(@"root/cimv2/Security/MicrosoftVolumeEncryption", "WQL", "SELECT * FROM Win32_EncryptableVolume");

                    if (cimInstances != null)
                    {
                        foreach (CimInstance cimInstance in cimInstances)
                        {
                            EncryptableVolume encryptableVolume = new EncryptableVolume
                            {
                                ConversionStatus = (uint)cimInstance.CimInstanceProperties["ConversionStatus"].Value,
                                DeviceId = (string)cimInstance.CimInstanceProperties["DeviceID"].Value,
                                DriveLetter = (string)cimInstance.CimInstanceProperties["DriveLetter"].Value,
                                EncryptionMethod = (uint)cimInstance.CimInstanceProperties["EncryptionMethod"].Value,
                                IsVolumeInitializedForProtection = (bool)cimInstance.CimInstanceProperties["IsVolumeInitializedForProtection"].Value,
                                LockStatus = (uint)session.InvokeMethod(cimInstance, "GetLockStatus", null).ReturnValue.Value,
                                //PersistentVolumeID = (string)cimInstance.CimInstanceProperties["PersistentVolumeID"].Value,
                                ProtectionStatus = (uint)cimInstance.CimInstanceProperties["ProtectionStatus"].Value,
                                VolumeType = (uint)cimInstance.CimInstanceProperties["VolumeType"].Value,
                            };

                            encryptableVolumes.Add(encryptableVolume);
                        }
                    }
                }
            } catch (Exception e) // Querying a CimSession that was changed due to a drive being unplugged can throw an exception, so we're accounting for that.
            {
                logger.Warn($"Caught an error. Most likely due to unplugging or plugging a drive. This is fairly normal: {e}");
            }            

            return encryptableVolumes;
        }

        // Get OS drive free space
        public ulong GetOsVolumeFreeSpaceBytes()
        {
            ulong osVolumeFreeSpaceBytes = 0;

            // get list of volumes
            List<Volume> volumes = GetVolumes();

            if (volumes.Count() > 0)
            {
                foreach (Volume volume in volumes)
                {
                    string driveLetter = volume.DriveLetter.ToString();
                    string systemDriveLetter = App.computerInfo.SystemDrive.Substring(0, 1); // SystemDrive is in the "C:" format, so it's necessary to take the substring
                    if (driveLetter == systemDriveLetter)
                    {
                        osVolumeFreeSpaceBytes = volume.SizeRemaining;
                    }
                }
            }

            return osVolumeFreeSpaceBytes;
        }

        // END Data Gathering Methods

    }
}
