﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class Battery
    {
        public ushort Availability { get; set; }
        public ushort BatteryStatus { get; set; }
        public string Caption { get; set; }
        // public ushort Chemistry { get; set; }
        public string Description { get; set; }
        // public ulong DesignVoltage { get; set; }
        public string DeviceID { get; set; }
        // public ushort EstimatedChargeRemaining { get; set; } // Some batteries return this as null
        // public uint EstimatedRunTime { get; set; } // Some batteries return this as null
        public string Name { get; set; }
        // public ushort[] PowerManagementCapabilities{ get; set; }
        // public bool PowerManagementSupported{ get; set; }
        public string Status { get; set; }
    }
}
