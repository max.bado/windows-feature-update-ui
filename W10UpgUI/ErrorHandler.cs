﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class ErrorHandler
    {
        private MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
        private Settings settings;
        private SettingsValidator settingsValidator;

        private string logSharePath;
        private string logFile;

        // Logging
        private static NLog.Logger logger;

        public ErrorHandler()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            settings = App.settings;
            settingsValidator = new SettingsValidator();

            logSharePath = settings.GetSettingsItemValue("Other", "LogSharePath");
            logFile = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("Other", "LogFilePath"), true).ValidatedPath;
        }

        public void ThrowCriticalError(string errorLogText, string errorNoticeText1, string errorNoticeText2)
        {
            logger.Error($"CRITICAL ERROR: {errorLogText}");

            bool isUiSession = App.IsUiSession();
            if (isUiSession)
            {
                ShowErrorNotice(errorNoticeText1, errorNoticeText2);
            }
        }

        private void ShowErrorNotice(string errorNoticeText1, string errorNoticeText2)
        {
            logger.Info($"Showing Error Notice in the UI");
            App.mainWindow.Dispatcher.Invoke(() =>
            {
                App.mainWindow.MetroWindow_Notice($"{errorNoticeText1}", $"{errorNoticeText2}");
            });
        }

        public void CopyLogs(string description)
        {
            if (logSharePath != "")
            {
                string remoteLogFileName = $"{Environment.MachineName}_{description}_{DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss")}.log";
                string remoteLogFilePath = $"{logSharePath}\\{remoteLogFileName}";

                try
                {
                    System.IO.File.OpenWrite(remoteLogFilePath).Close();
                    logger.Info($"Successfully wrote to '{remoteLogFilePath}'");
                    System.IO.File.Delete(remoteLogFilePath);
                    System.IO.File.Copy(logFile, remoteLogFilePath);
                }
                catch (Exception e)
                {
                    logger.Error($"Error writing/copying to '{remoteLogFilePath}'");
                    logger.Error(e);
                }
            }
            else
            {
                logger.Info($"LogSharePath was not specified. Logs won't be copied to a remote share.");
            }
        }
    }
}
