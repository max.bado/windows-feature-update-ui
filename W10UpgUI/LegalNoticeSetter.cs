﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace W10UpgUI
{
    public class LegalNoticeSetter
    {

        // Logging
        private static NLog.Logger logger;

        public LegalNoticeSetter()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void SetLegalNoticeRegKeys(string legalNoticeCaption, string legalNoticeText)
        {
            logger.Info($"Start setting Legal Notice registry keys.");

            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            }
            else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                string regKeyPath = @"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";
                using (RegistryKey regKey = regLocalMachine.OpenSubKey(regKeyPath, true))
                {
                    if (legalNoticeText != null)
                    {
                        logger.Warn($"Setting '{regKey.Name} | legalnoticetext' with value '{legalNoticeText}'");
                        regKey.SetValue("legalnoticetext", legalNoticeText);
                    }
                    else
                    {
                        logger.Error($"'legalnoticetext' is not defined in the settings file. Nothing to set.");
                    }

                    if (legalNoticeCaption != null)
                    {
                        logger.Warn($"Setting '{regKey.Name} | legalnoticecaption' with value '{legalNoticeCaption}'");
                        regKey.SetValue("legalnoticecaption", legalNoticeCaption);
                    }
                    else
                    {
                        logger.Error($"'legalnoticecaption' is not defined in the settings file. Nothing to set.");
                    }
                }
            }
        }
    }
}
