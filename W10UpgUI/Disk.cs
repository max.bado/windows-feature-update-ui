﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class Disk
    {
        public ushort BusType { get; set; }
        public string FriendlyName { get; set; }
        public bool IsBoot { get; set; }
        public bool IsOffline { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsSystem { get; set; }
        public string Model { get; set; }
        public uint Number { get; set; }
        public string ObjectId { get; set; }
        public ushort[] OperationalStatus { get; set; }
        public ushort PartitionStyle { get; set; } // 0 - Unknown; 1 - MBR; 2 - GPT
        public string UniqueId { get; set; }
    }
}
