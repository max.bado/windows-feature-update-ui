﻿using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace W10UpgUI
{
    class ScheduledTaskRunner
    {
        private Settings settings;
        public static NLog.Logger logger;

        public ScheduledTaskRunner()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            settings = App.settings;
        }

        public void UnregisterScheduledTask(string scheduledTaskXmlPath)
        {
            logger.Info($"BEGIN unregistering Scheduled Task");

            ScheduledTaskDetails scheduledTaskDetails = new ScheduledTaskDetails(scheduledTaskXmlPath);

            if(scheduledTaskDetails != null)
            {
                logger.Info($"Removing Scheduled Task '{scheduledTaskDetails.ScheduledTaskName}'");
                TaskService.Instance.RootFolder.DeleteTask(scheduledTaskDetails.ScheduledTaskName, false);
            }
        }

        public void RegisterScheduledTask(string scheduledTaskName, string scheduledTaskXmlPath)
        {
            logger.Info($"BEGIN registering Scheduled Task");
            
            // Validate path
            //scheduledTaskXmlPath = App.GetAbsolutePath(scheduledTaskXmlPath);
            //string scheduledTaskName = settings.Other["ScheduledTaskName"].Value;

            // Validate the Task Scheduler XML to make sure the command file path is accessible
            ScheduledTaskDetails scheduledTaskDetails = new ScheduledTaskDetails(scheduledTaskXmlPath);
            //scheduledTaskDetails.CreateScheduledTaskDetails(scheduledTaskXmlPath);
            
            if (scheduledTaskDetails != null)
            {
                if (scheduledTaskDetails.ScheduledTaskModifiedFlag == true)
                {
                    // We need to manually create the Scheduled Task with the appropriate validated path of the command
                    logger.Warn($"The Scheduled Task command file path was not a globally accessible absolute path and had to be modified. Manually creating and registering Scheduled Task with modified command file path.");

                    TaskDefinition taskDefinition = TaskService.Instance.NewTaskFromFile(scheduledTaskXmlPath);
                    ActionCollection actions = taskDefinition.Actions;
                    foreach (ExecAction action in actions)
                    {
                        logger.Info($"Old path: {action.Path}");
                        action.SetValidatedPath(action.Path, true);
                        logger.Info($"New path: {action.Path}");

                        logger.Warn($"Removing action '{action.Path} {action.Arguments}' from the Scheduled Task definition.");
                        taskDefinition.Actions.Remove(action);
                    }

                    foreach (ScheduledTaskAction action in scheduledTaskDetails.ScheduledTaskActions)
                    {
                        if (action.ActionWorkingDirectory != null)
                        {
                            logger.Warn($"Adding action '{action.ActionCommand} {action.ActionArguments}' with working directory '{action.ActionWorkingDirectory}' to the Scheduled Task definition");
                            taskDefinition.Actions.Add(action.ActionCommand, action.ActionArguments, action.ActionWorkingDirectory);
                        }
                        else
                        {
                            logger.Warn($"Adding action '{action.ActionCommand} {action.ActionArguments}' to the Scheduled Task definition");
                            taskDefinition.Actions.Add(action.ActionCommand, action.ActionArguments);
                        }
                    }

                    if ((scheduledTaskName == null) && (scheduledTaskDetails.ScheduledTaskName == null))
                    {
                        scheduledTaskName = Guid.NewGuid().ToString();
                    } else if (scheduledTaskName == null)
                    {
                        scheduledTaskName = scheduledTaskDetails.ScheduledTaskName;
                    }

                    logger.Warn($"Creating Scheduled Task named '{scheduledTaskName}'");
                    TaskService.Instance.RootFolder.RegisterTaskDefinition(scheduledTaskName, taskDefinition);
                }
                else
                {
                    logger.Info($"Creating Scheduled Task named '{scheduledTaskName}' from '{scheduledTaskXmlPath}'");
                    TaskService.Instance.RootFolder.ImportTask(scheduledTaskName, scheduledTaskXmlPath);
                }
            }
            else
            {
                logger.Error($"Unable to parse Scheduled Task XML.");
            }
        }



    }


}
