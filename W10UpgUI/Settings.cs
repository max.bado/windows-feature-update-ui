﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace W10UpgUI
{
    public class Settings
    {
        //public dynamic Customizations { get; set; }
        //public dynamic Details { get; set; }
        //public dynamic Hardware { get; set; }
        //public dynamic Network { get; set; }
        //public dynamic TaskSequence { get; set; }
        //public dynamic PostGUI { get; set; }
        //public dynamic Other { get; set; }

        private JObject settingsContent;
        private List<SettingsGroup> settingsGroups;
        private string helpdeskEmail;
        private string helpdeskName;
        private string helpdeskNumber;
        private string contactHelpDesk;

        // Logging
        private static NLog.Logger logger;

        public Settings(string settingsFilePath)
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            logger.Info($"Initializaing settings file: '{settingsFilePath}'");
            settingsGroups = new List<SettingsGroup>();

            // Expand file path variables and validate settings file path
            string settingsFileFullPath;
            logger.Info($"Attempting to expand any environment variables...");
            settingsFileFullPath = Environment.ExpandEnvironmentVariables(settingsFilePath);
            logger.Info($"Path post variable expansion: '{settingsFileFullPath}'");
            FileInfo fileInfo = new FileInfo(settingsFileFullPath);

            if(fileInfo.Exists)
            {
                try
                {
                    settingsContent = JObject.Parse(File.ReadAllText(fileInfo.FullName));
                    GenerateSettingsGroups();

                }
                catch (Exception e)
                {
                    logger.Error(e);
                    logger.Error($"Failed to parse the settings file");
                }
            } else if(File.Exists($"{App.currentApplicationDirectory}\\{settingsFilePath}"))
            {
                fileInfo = new FileInfo($"{App.currentApplicationDirectory}\\{settingsFilePath}");
                try
                {
                    settingsContent = JObject.Parse(File.ReadAllText(fileInfo.FullName));
                    GenerateSettingsGroups();

                }
                catch (Exception e)
                {
                    logger.Error(e);
                    logger.Error($"Failed to parse the settings file");
                }
            } else
            {
                logger.Error($"Unable to locate {settingsFilePath} in any context");
            }

            helpdeskEmail = this.GetSettingsItemValue("Customizations", "HelpdeskEmail");
            helpdeskName = this.GetSettingsItemValue("Customizations", "HelpdeskName");
            helpdeskNumber = this.GetSettingsItemValue("Customizations", "HelpdeskNumber");
            contactHelpDesk = $"Please contact the {helpdeskName} at {helpdeskNumber} or email {helpdeskEmail}.";
        }

        private void GenerateSettingsGroups()
        {
            logger.Info($"Generating Settings Group List");
            foreach(JProperty group in settingsContent.Properties())
            {
                SettingsGroup settingsGroup = new SettingsGroup();
                List<SettingsItem> settingsItems = new List<SettingsItem>();
                settingsGroup.GroupName = group.Name;
                logger.Info($"Creating settings group: '{settingsGroup.GroupName}'");

                JToken jObjectGroup = settingsContent[group.Name];
                foreach(JProperty item in jObjectGroup)
                {
                    SettingsItem settingsItem = new SettingsItem();
                    settingsItem.ItemName = item.Name;
                    settingsItem.ItemValue = (string)settingsContent[group.Name][item.Name];
                    settingsItem.ItemGroup = settingsGroup.GroupName;
                    logger.Info($"Adding settings item, '{settingsItem.ItemName}' with value, '{settingsItem.ItemValue}' to group '{settingsItem.ItemGroup}'");

                    settingsItems.Add(settingsItem);
                }

                settingsGroup.GroupItems = settingsItems;
                settingsGroups.Add(settingsGroup);
            }        
        }

        public string GetSettingsItemValue(string groupName, string itemName)
        {
            logger.Info($"Retrieving value for settings item, '{itemName}', from settings group, '{groupName}'");

            string settingsItemValue = null;
            var settingsGroupQuery = from x in settingsGroups
                                     where x.GroupName == groupName
                                     select x;
            if(settingsGroupQuery.Count() > 0)
            {
                foreach (SettingsGroup settingsGroup in settingsGroupQuery)
                {

                    var settingsItemQuery = from y in settingsGroup.GroupItems
                                            where y.ItemName == itemName
                                            select y;
                    if(settingsItemQuery.Count() > 0)
                    {
                        foreach (SettingsItem settingsItem in settingsItemQuery)
                        {
                            settingsItemValue = settingsItem.ItemValue;
                        }
                    } else
                    {
                        HandleError($"Unable to find item, '{itemName}' in group '{groupName}' within the settings file. The settings file may have been modified and is now formatted incorrectly...", contactHelpDesk);

                    }
                }
            } else
            {
                HandleError($"Unable to find group, '{groupName}', in the settings file. The settings file may have been modified and is now formatted incorrectly...", contactHelpDesk);

            }

            return settingsItemValue;
        }

        public SettingsItem GetSettingsItem(string groupName, string itemName)
        {
            logger.Info($"Retrieving settings item, '{itemName}', from settings group, '{groupName}'");
            SettingsItem settingsItemResult = new SettingsItem();
            var settingsGroupQuery = from x in settingsGroups
                                     where x.GroupName == groupName
                                     select x;
            if(settingsGroupQuery.Count() > 0)
            {
                foreach (SettingsGroup settingsGroup in settingsGroupQuery)
                {

                    var settingsItemQuery = from y in settingsGroup.GroupItems
                                            where y.ItemName == itemName
                                            select y;
                    if(settingsItemQuery.Count() > 0)
                    {
                        foreach (SettingsItem settingsItem in settingsItemQuery)
                        {
                            settingsItemResult = settingsItem;
                        }
                    } else
                    {                        
                        HandleError($"Unable to find item, '{itemName}' in group '{groupName}' within the settings file. The settings file may have been modified and is now formatted incorrectly...", contactHelpDesk);
                    }
                }
            } else
            {
                HandleError($"Unable to find group, '{groupName}', in the settings file. The settings file may have been modified and is now formatted incorrectly...", contactHelpDesk);

            }

            return settingsItemResult;
        }

        private void HandleError(string errorText1, string errorText2)
        {

            logger.Error($"{errorText1}");
            logger.Error($"{errorText2}");

            bool isUiSession = App.IsUiSession();
            if(isUiSession)
            {
                App.mainWindow.MetroWindow_Notice($"{errorText1}", $"{errorText2}");
            }
        }
    }
}
