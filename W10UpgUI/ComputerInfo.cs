﻿using Microsoft.Management.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class ComputerInfo
    {
        public string Caption { get; set; }
        public string Version { get; set; }
        public uint OperatingSystemSKU { get; set; }
        public string SystemDrive { get; set; }
        public string OSArchitecture { get; set; }
        public int BuildNumber { get; set; }
        public ulong TotalPhysicalMemory { get; set; }
        public string Name { get; set; }
        public string ChassisSKUNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public ushort[] ChassisTypes { get; set; }

        public ComputerInfo()
        {
            ServiceController service = new ServiceController("WinRM");
            if (service.Status != ServiceControllerStatus.Running)
            {
                service.Start();
            }

            service.Close();
            service.Dispose();


            GetOperatingSystem();
            GetComputerSystem();
            GetSystemEnclosure();
        }

        private void GetOperatingSystem()
        {
            string computerName = Environment.MachineName;

            // Create CIM session
            using (CimSession session = CimSession.Create(computerName))
            {
                // Query CIM_OperatingSystem class
                var cimInstanceList = session.QueryInstances(@"root/cimv2", "WQL", "SELECT Caption, Version, SystemDrive, OSArchitecture, OperatingSystemSKU, BuildNumber FROM Win32_OperatingSystem");

                if (cimInstanceList != null)
                {
                    foreach (CimInstance cimInstance in cimInstanceList)
                    {
                        if (cimInstance.CimInstanceProperties["Caption"].Value != null)
                        {
                            Caption = (string)cimInstance.CimInstanceProperties["Caption"].Value;
                        }

                        if (cimInstance.CimInstanceProperties["Version"].Value != null)
                        {
                            Version = (string)cimInstance.CimInstanceProperties["Version"].Value;
                        }

                        if (cimInstance.CimInstanceProperties["SystemDrive"].Value != null)
                        {
                            SystemDrive = (string)cimInstance.CimInstanceProperties["SystemDrive"].Value;
                        }

                        if (cimInstance.CimInstanceProperties["OSArchitecture"].Value != null)
                        {
                            OSArchitecture = (string)cimInstance.CimInstanceProperties["OSArchitecture"].Value;
                        }

                        if (cimInstance.CimInstanceProperties["OperatingSystemSKU"].Value != null)
                        {
                            OperatingSystemSKU = (uint)cimInstance.CimInstanceProperties["OperatingSystemSKU"].Value;
                        }

                        if (cimInstance.CimInstanceProperties["BuildNumber"].Value != null)
                        {
                            BuildNumber = int.Parse((string)cimInstance.CimInstanceProperties["BuildNumber"].Value);
                        }
                    }
                }
            }
        }

        private void GetComputerSystem()
        {
            string computerName = Environment.MachineName;

            // Create CIM session
            using (CimSession session = CimSession.Create(computerName))
            {
                // Query CIM_OperatingSystem class, returns CimInstances
                var cimInstanceList = session.QueryInstances(@"root/cimv2", "WQL", "SELECT Name, ChassisSKUNumber, Manufacturer, Model, TotalPhysicalMemory FROM Win32_ComputerSystem");

                foreach (CimInstance cimInstance in cimInstanceList)
                {
                    if (cimInstance.CimInstanceProperties["Name"].Value != null)
                    {
                        Name = (string)cimInstance.CimInstanceProperties["Name"].Value;
                    }

                    if (cimInstance.CimInstanceProperties["ChassisSKUNumber"].Value != null)
                    {
                        ChassisSKUNumber = (string)cimInstance.CimInstanceProperties["ChassisSKUNumber"].Value;
                    }

                    if (cimInstance.CimInstanceProperties["Manufacturer"].Value != null)
                    {
                        Manufacturer = (string)cimInstance.CimInstanceProperties["Manufacturer"].Value;
                    }

                    if (cimInstance.CimInstanceProperties["Model"].Value != null)
                    {
                        Model = (string)cimInstance.CimInstanceProperties["Model"].Value;
                    }

                    if (cimInstance.CimInstanceProperties["TotalPhysicalMemory"].Value != null)
                    {
                        TotalPhysicalMemory = (ulong)cimInstance.CimInstanceProperties["TotalPhysicalMemory"].Value;
                    }
                }
            }
        }

        private void GetSystemEnclosure()
        {
            string computerName = Environment.MachineName;

            // Create CIM session
            using (CimSession session = CimSession.Create(computerName))
            {
                // Query CIM_OperatingSystem class, returns CimInstances
                var cimInstanceList = session.QueryInstances(@"root/cimv2", "WQL", "SELECT SerialNumber, ChassisTypes FROM Win32_SystemEnclosure");

                foreach (CimInstance cimInstance in cimInstanceList)
                {
                    if (cimInstance.CimInstanceProperties["SerialNumber"].Value != null)
                    {
                        SerialNumber = (string)cimInstance.CimInstanceProperties["SerialNumber"].Value;
                    }

                    if (cimInstance.CimInstanceProperties["ChassisTypes"].Value != null)
                    {
                        ChassisTypes = (ushort[])cimInstance.CimInstanceProperties["ChassisTypes"].Value;
                    }
                }
            }
        }
    }
}
