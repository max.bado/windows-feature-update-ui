﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    class DataWatcher
    {
        private ManagementEventWatcher diskCreationWatcher;
        private ManagementEventWatcher diskDeletionWatcher;
        private List<EventArrivedEventArgs> diskCreationEventList;
        private List<EventArrivedEventArgs> diskDeletionEventList;
        private ManagementEventWatcher encryptableVolumeCreationWatcher;
        private ManagementEventWatcher encryptableVolumeDeletionWatcher;
        private ManagementEventWatcher encryptableVolumeModificationWatcher;
        private List<EventArrivedEventArgs> encryptableVolumeCreationEventList;
        private List<EventArrivedEventArgs> encryptableVolumeDeletionEventList;
        private List<EventArrivedEventArgs> encryptableVolumeModificationEventList;
        private ManagementEventWatcher netAdapterCreationWatcher;
        private ManagementEventWatcher netAdapterDeletionWatcher;
        private ManagementEventWatcher netAdapterModificationWatcher;
        private List<EventArrivedEventArgs> netAdapterCreationEventList;
        private List<EventArrivedEventArgs> netAdapterDeletionEventList;
        private List<EventArrivedEventArgs> netAdapterModificationEventList;
        private ManagementEventWatcher batteryCreationWatcher;
        private ManagementEventWatcher batteryDeletionWatcher;
        private ManagementEventWatcher batteryModificationWatcher;
        private List<EventArrivedEventArgs> batteryCreationEventList;
        private List<EventArrivedEventArgs> batteryDeletionEventList;
        private List<EventArrivedEventArgs> batteryModificationEventList;

        private List<ManagementEventWatcher> managementEventWatchers;

        public static NLog.Logger logger;

        public DataWatcher()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            managementEventWatchers = new List<ManagementEventWatcher>();

            diskCreationEventList = new List<EventArrivedEventArgs>();
            diskDeletionEventList = new List<EventArrivedEventArgs>();
            encryptableVolumeCreationEventList = new List<EventArrivedEventArgs>();
            encryptableVolumeDeletionEventList = new List<EventArrivedEventArgs>();
            encryptableVolumeModificationEventList = new List<EventArrivedEventArgs>();
            netAdapterCreationEventList = new List<EventArrivedEventArgs>();
            netAdapterDeletionEventList = new List<EventArrivedEventArgs>();
            netAdapterModificationEventList = new List<EventArrivedEventArgs>();
            batteryCreationEventList = new List<EventArrivedEventArgs>();
            batteryDeletionEventList = new List<EventArrivedEventArgs>();
            batteryModificationEventList = new List<EventArrivedEventArgs>();
        }

        public void StopAllWatchers()
        {
            foreach(ManagementEventWatcher managementEventWatcher in managementEventWatchers)
            {
                logger.Warn($"Stopping and disposing watcher defined by query string: {managementEventWatcher.Query.QueryString}");
                managementEventWatcher.Stop();
                managementEventWatcher.Dispose();
            }
        }

        // BEGIN Disk Watcher Methods

        // BEGIN Disk Creation Watcher
        public void StartDiskCreationWatcher()
        {
            logger.Info($"Creating Disk Creation Watcher...");
            if (diskCreationWatcher != null)
            {
                //logger.Warn($"DiskCreationWatcher is already running.");
                diskCreationWatcher.Start();
            } else
            {
                string eventClassName = "__InstanceCreationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'MSFT_Disk'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/microsoft/windows/storage");
                diskCreationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                diskCreationWatcher.EventArrived += DiskCreationWatcher_EventArrived;
                diskCreationWatcher.Start();
                managementEventWatchers.Add(diskCreationWatcher);
            }
        }

        private void DiskCreationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected creation of a new MSFT_Disk instance");
            //ManagementBaseObject x = (ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value;
            //logger.Info(x.Properties["SerialNumber"].Value);
            //logger.Warn($"Current diskCreationEventList.Count(): {diskCreationEventList.Count()}");
            this.diskCreationEventList.Add(e);
            //logger.Warn($"New diskCreationEventList.Count(): {diskCreationEventList.Count()}");

        }

        public void StopDiskCreationWatcher()
        {
            if(diskCreationWatcher == null)
            {
                logger.Warn($"DiskCreationWatcher is already stopped and disposed.");
            } else
            {
                logger.Info($"Stopping and disposing of DiskCreationWatcher");
                diskCreationWatcher.Stop();
                diskCreationWatcher.Dispose();
                managementEventWatchers.Remove(diskCreationWatcher);
            }
        }
        // END Disk Creation Watcher

        // BEGIN Disk Deletion Watcher
        public void StartDiskDeletionWatcher()
        {
            logger.Info($"Creating Disk Deletion Watcher...");
            if (diskDeletionWatcher != null)
            {
                diskDeletionWatcher.Start();
                //logger.Warn($"DiskDeletionWatcher is already running.");
            }
            else
            {
                string eventClassName = "__InstanceDeletionEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'MSFT_Disk'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/microsoft/windows/storage");
                diskDeletionWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                diskDeletionWatcher.EventArrived += DiskDeletionWatcher_EventArrived;
                diskDeletionWatcher.Start();
                managementEventWatchers.Add(diskDeletionWatcher);
            }
        }

        private void DiskDeletionWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected deletion of an MSFT_Disk instance");
            //logger.Warn($"Current diskDeletionEventList.Count(): {diskDeletionEventList.Count()}");
            this.diskDeletionEventList.Add(e);
            //logger.Warn($"New diskDeletionEventList.Count(): {diskDeletionEventList.Count()}");
        }

        public void StopDiskDeletionWatcher()
        {
            if (diskDeletionWatcher == null)
            {
                logger.Warn($"DiskDeletionWatcher is already stopped and disposed");
            }
            else
            {
                logger.Info($"Stopping and disposing of DiskDeletionWatcher");
                diskDeletionWatcher.Stop();
                diskDeletionWatcher.Dispose();
                managementEventWatchers.Remove(diskDeletionWatcher);
            }
        }
        // END Disk Deletion Watcher

        // BEGIN Disk Watcher Accessor Methods
        public List<EventArrivedEventArgs> getDiskCreationEventList()
        {
            return this.diskCreationEventList;
        }

        public List<EventArrivedEventArgs> getDiskDeletionEventList()
        {
            return this.diskDeletionEventList;
        }
        // END Disk Watcher Accessor Methods

        // END Disk Watcher Methods

        // BEGIN Encryptable Volume Watcher Methods

        // BEGIN EncryptableVolume Creation Watcher
        public void StartEncryptableVolumeCreationWatcher()
        {
            logger.Info($"Creating Encryptable Volume Creation Watcher...");
            if (encryptableVolumeCreationWatcher != null)
            {
                encryptableVolumeCreationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceCreationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_EncryptableVolume'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2/Security/MicrosoftVolumeEncryption");
                encryptableVolumeCreationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                encryptableVolumeCreationWatcher.EventArrived += EncryptableVolumeCreationWatcher_EventArrived;
                encryptableVolumeCreationWatcher.Start();
                managementEventWatchers.Add(encryptableVolumeCreationWatcher);
            }
        }

        private void EncryptableVolumeCreationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected creation of a new Win32_EncryptableVolume instance");

            this.encryptableVolumeCreationEventList.Add(e);
        }

        public void StopEncryptableVolumeCreationWatcher()
        {
            if (encryptableVolumeCreationWatcher == null)
            {
                logger.Warn($"EncryptableVolumeCreationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of EncryptableVolumeCreationWatcher");
                encryptableVolumeCreationWatcher.Stop();
                encryptableVolumeCreationWatcher.Dispose();
                managementEventWatchers.Remove(encryptableVolumeCreationWatcher);
            }
        }
        // END EncryptableVolume Creation Watcher

        // BEGIN EncryptableVolume Deletion Watcher
        public void StartEncryptableVolumeDeletionWatcher()
        {
            logger.Info($"Creating Encryptable Volume Deletion Watcher...");
            if (encryptableVolumeDeletionWatcher != null)
            {
                encryptableVolumeDeletionWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceDeletionEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_EncryptableVolume'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2/Security/MicrosoftVolumeEncryption");
                encryptableVolumeDeletionWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                encryptableVolumeDeletionWatcher.EventArrived += EncryptableVolumeDeletionWatcher_EventArrived;
                encryptableVolumeDeletionWatcher.Start();
                managementEventWatchers.Add(encryptableVolumeDeletionWatcher);
            }
        }

        private void EncryptableVolumeDeletionWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected deletion of a Win32_EncryptableVolume instance");

            this.encryptableVolumeDeletionEventList.Add(e);

        }

        public void StopEncryptableVolumeDeletionWatcher()
        {
            if (encryptableVolumeDeletionWatcher == null)
            {
                logger.Warn($"EncryptableVolumeDeletionWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of EncryptableVolumeDeletionWatcher");
                encryptableVolumeDeletionWatcher.Stop();
                encryptableVolumeDeletionWatcher.Dispose();
                managementEventWatchers.Remove(encryptableVolumeDeletionWatcher);
            }
        }
        // END EncryptableVolume Deletion Watcher

        // BEGIN EncryptableVolume Modification Watcher
        public void StartEncryptableVolumeModificationWatcher()
        {
            logger.Info($"Creating Encryptable Volume Modification Watcher...");
            if (encryptableVolumeModificationWatcher != null)
            {
                encryptableVolumeModificationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceModificationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_EncryptableVolume'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2/Security/MicrosoftVolumeEncryption");
                encryptableVolumeModificationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                encryptableVolumeModificationWatcher.EventArrived += EncryptableVolumeModificationWatcher_EventArrived;
                encryptableVolumeModificationWatcher.Start();
                managementEventWatchers.Add(encryptableVolumeModificationWatcher);
            }
        }

        private void EncryptableVolumeModificationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected modification of a new Win32_EncryptableVolume instance");

            this.encryptableVolumeModificationEventList.Add(e);
        }

        public void StopEncryptableVolumeModificationWatcher()
        {
            if (encryptableVolumeModificationWatcher == null)
            {
                logger.Warn($"EncryptableVolumeModificationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of EncryptableVolumeModificationWatcher");
                encryptableVolumeModificationWatcher.Stop();
                encryptableVolumeModificationWatcher.Dispose();
                managementEventWatchers.Remove(encryptableVolumeModificationWatcher);
            }
        }
        // END EncryptableVolume Modification Watcher

        // BEGIN EncryptableVolume Accessor Methods
        public List<EventArrivedEventArgs> getEncryptableVolumeCreationEventList()
        {
            return this.encryptableVolumeCreationEventList;
        }

        public List<EventArrivedEventArgs> getEncryptableVolumeDeletionEventList()
        {
            return this.encryptableVolumeDeletionEventList;
        }

        public List<EventArrivedEventArgs> getEncryptableVolumeModificationEventList()
        {
            return this.encryptableVolumeModificationEventList;
        }
        // END EncryptableVolume Accessor Methods

        // END BitLocker Watcher Methods

        // BEGIN NetAdapter Watcher Methods

        // BEGIN NetAdapter Creation Watcher
        public void StartNetAdapterCreationWatcher()
        {
            logger.Info($"Creating NetAdapter Creation Watcher...");
            if (netAdapterCreationWatcher != null)
            {
                netAdapterCreationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceCreationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'MSFT_NetAdapter'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/StandardCimv2");
                netAdapterCreationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                netAdapterCreationWatcher.EventArrived += NetAdapterCreationWatcher_EventArrived;
                netAdapterCreationWatcher.Start();
                managementEventWatchers.Add(netAdapterCreationWatcher);
            }
        }

        private void NetAdapterCreationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected creation of a new MSFT_NetAdapter instance");

            this.netAdapterCreationEventList.Add(e);
        }

        public void StopNetAdapterCreationWatcher()
        {
            if (netAdapterCreationWatcher == null)
            {
                logger.Warn($"NetAdapterCreationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of NetAdapterCreationWatcher");
                netAdapterCreationWatcher.Stop();
                netAdapterCreationWatcher.Dispose();
                managementEventWatchers.Remove(netAdapterCreationWatcher);
            }
        }
        // END NetAdapter Creation Watcher

        // BEGIN NetAdapter Deletion Watcher
        public void StartNetAdapterDeletionWatcher()
        {
            logger.Info($"Creating NetAdapter Deletion Watcher...");
            if (netAdapterDeletionWatcher != null)
            {
                netAdapterDeletionWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceDeletionEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'MSFT_NetAdapter'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/StandardCimv2");
                netAdapterDeletionWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                netAdapterDeletionWatcher.EventArrived += NetAdapterDeletionWatcher_EventArrived;
                netAdapterDeletionWatcher.Start();
                managementEventWatchers.Add(netAdapterDeletionWatcher);
            }
        }

        private void NetAdapterDeletionWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected deletion of a MSFT_NetAdapter instance");

            this.netAdapterDeletionEventList.Add(e);

        }

        public void StopNetAdapterDeletionWatcher()
        {
            if (netAdapterDeletionWatcher == null)
            {
                logger.Warn($"NetAdapterDeletionWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of NetAdapterDeletionWatcher");
                netAdapterDeletionWatcher.Stop();
                netAdapterDeletionWatcher.Dispose();
                managementEventWatchers.Remove(netAdapterDeletionWatcher);
            }
        }
        // END NetAdapter Deletion Watcher

        // BEGIN NetAdapter Modification Watcher
        public void StartNetAdapterModificationWatcher()
        {
            logger.Info($"Creating NetAdapter Modification Watcher...");
            if (netAdapterModificationWatcher != null)
            {
                netAdapterModificationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceModificationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'MSFT_NetAdapter'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/StandardCimv2");
                netAdapterModificationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                netAdapterModificationWatcher.EventArrived += NetAdapterModificationWatcher_EventArrived;
                netAdapterModificationWatcher.Start();
                managementEventWatchers.Add(netAdapterModificationWatcher);
            }
        }

        private void NetAdapterModificationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected modification of a MSFT_NetAdapter instance");

            this.netAdapterModificationEventList.Add(e);
        }

        public void StopNetAdapterModificationWatcher()
        {
            if (netAdapterModificationWatcher == null)
            {
                logger.Warn($"NetAdapterModificationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of NetAdapterModificationWatcher");
                netAdapterModificationWatcher.Stop();
                netAdapterModificationWatcher.Dispose();
                managementEventWatchers.Remove(netAdapterModificationWatcher);
            }
        }
        // END NetAdapter Modification Watcher

        // BEGIN NetAdapter Accessor Methods
        public List<EventArrivedEventArgs> getNetAdapterCreationEventList()
        {
            return this.netAdapterCreationEventList;
        }

        public List<EventArrivedEventArgs> getNetAdapterDeletionEventList()
        {
            return this.netAdapterDeletionEventList;
        }

        public List<EventArrivedEventArgs> getNetAdapterModificationEventList()
        {
            return this.netAdapterModificationEventList;
        }
        // END NetAdapter Accessor Methods

        // END Network Watcher Methods

        // BEGIN Battery Watcher Methods
        // BEGIN Battery Creation Watcher
        public void StartBatteryCreationWatcher()
        {
            logger.Info($"Creating Battery Creation Watcher...");
            if (batteryCreationWatcher != null)
            {
                batteryCreationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceCreationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_Battery'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2");
                batteryCreationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                batteryCreationWatcher.EventArrived += BatteryCreationWatcher_EventArrived;
                batteryCreationWatcher.Start();
                managementEventWatchers.Add(batteryCreationWatcher);
            }
        }

        private void BatteryCreationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected creation of a new Win32_Battery instance");

            this.batteryCreationEventList.Add(e);
        }

        public void StopBatteryCreationWatcher()
        {
            if (batteryCreationWatcher == null)
            {
                logger.Warn($"BatteryCreationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of BatteryCreationWatcher");
                batteryCreationWatcher.Stop();
                batteryCreationWatcher.Dispose();
                managementEventWatchers.Remove(batteryCreationWatcher);
            }
        }
        // END Battery Creation Watcher

        // BEGIN Battery Deletion Watcher
        public void StartBatteryDeletionWatcher()
        {
            logger.Info($"Creating Battery Deletion Watcher...");
            if (batteryDeletionWatcher != null)
            {
                batteryDeletionWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceDeletionEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_Battery'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2");
                batteryDeletionWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                batteryDeletionWatcher.EventArrived += BatteryDeletionWatcher_EventArrived;
                batteryDeletionWatcher.Start();
                managementEventWatchers.Add(batteryDeletionWatcher);
            }
        }

        private void BatteryDeletionWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected deletion of a Win32_Battery instance");

            this.batteryDeletionEventList.Add(e);

        }

        public void StopBatteryDeletionWatcher()
        {
            if (batteryDeletionWatcher == null)
            {
                logger.Warn($"BatteryDeletionWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of BatteryDeletionWatcher");
                batteryDeletionWatcher.Stop();
                batteryDeletionWatcher.Dispose();
                managementEventWatchers.Remove(batteryDeletionWatcher);
            }
        }
        // END Battery Deletion Watcher

        // BEGIN Battery Modification Watcher
        public void StartBatteryModificationWatcher()
        {
            logger.Info($"Creating Battery Modification Watcher...");
            if (batteryModificationWatcher != null)
            {
                batteryModificationWatcher.Start();
            }
            else
            {
                string eventClassName = "__InstanceModificationEvent";
                TimeSpan timeSpan = TimeSpan.FromMilliseconds(1000);
                string condition = "TargetInstance isa 'Win32_Battery'";
                WqlEventQuery wqlEventQuery = new WqlEventQuery(eventClassName, timeSpan, condition);

                ManagementScope managementScope = new ManagementScope(@"root/cimv2");
                batteryModificationWatcher = new ManagementEventWatcher(managementScope, wqlEventQuery);

                batteryModificationWatcher.EventArrived += BatteryModificationWatcher_EventArrived;
                batteryModificationWatcher.Start();
                managementEventWatchers.Add(batteryModificationWatcher);
            }
        }

        private void BatteryModificationWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Info($"Detected modification of a Win32_Battery instance");

            // Log the event only if the BatteryStatus property has changed
            ManagementBaseObject previousInstance = (ManagementBaseObject)e.NewEvent.Properties["PreviousInstance"].Value;
            ManagementBaseObject targetInstance = (ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value;
            
            logger.Info($"PreviousInstance BatteryStatus: {previousInstance.Properties["BatteryStatus"].Value}");
            logger.Info($"TargetInstance BatteryStatus: {targetInstance.Properties["BatteryStatus"].Value}");

            if((ushort)previousInstance.Properties["BatteryStatus"].Value != (ushort)targetInstance.Properties["BatteryStatus"].Value)
            {
                logger.Warn($"BatteryStatus changed. Log the instance");
                this.batteryModificationEventList.Add(e);
            } else
            {
                logger.Info($"No change to BatteryStatus. Ignoring this modification");
            }
            
        }

        public void StopBatteryModificationWatcher()
        {
            if (batteryModificationWatcher == null)
            {
                logger.Warn($"BatteryModificationWatcher does not exist");
            }
            else
            {
                logger.Info($"Stopping and disposing of BatteryModificationWatcher");
                batteryModificationWatcher.Stop();
                batteryModificationWatcher.Dispose();
                managementEventWatchers.Remove(batteryModificationWatcher);
            }
        }
        // END Battery Modification Watcher

        // BEGIN Battery Watcher Accessor Methods
        public List<EventArrivedEventArgs> getBatteryCreationEventList()
        {
            return this.batteryCreationEventList;
        }

        public List<EventArrivedEventArgs> getBatteryDeletionEventList()
        {
            return this.batteryDeletionEventList;
        }

        public List<EventArrivedEventArgs> getBatteryModificationEventList()
        {
            return this.batteryModificationEventList;
        }
        // END Battery Watcher Accessor Methods

        // END Battery Watcher
    }
}
