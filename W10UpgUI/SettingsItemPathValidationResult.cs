﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class SettingsItemPathValidationResult
    {
        public bool Result { get; set; }
        public string ValidatedPath { get; set; }
        public SettingsItem SettingsItem { get; set; }
    }
}
