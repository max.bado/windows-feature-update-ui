﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W10UpgUI
{
    public class SettingsGroup
    {
        public string GroupName { get; set; }
        public List<SettingsItem> GroupItems { get; set; }
    }
}
