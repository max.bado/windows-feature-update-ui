﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using smsclictr.automation;
using Microsoft.Win32;

namespace W10UpgUI
{
    public class TaskSequenceRunner
    {
        private SMSClient smsClient;
        private Settings settings;
        private bool tsManagerStarted;        
        private string webServiceURI;
        private string secretKey;
        private string processName;
        private string logSharePath;
        private string logFile;
        private ScheduledTaskRunner scheduledTaskRunner;
        private LegalNoticeSetter legalNoticeSetter;
        private Cleanup cleanup;
        private SettingsValidator settingsValidator;
        private ManagementEventWatcher startWatch;
        private ErrorHandler errorHandler;

        // Logging
        private static NLog.Logger logger;

        public TaskSequenceRunner()
        {
            // Initialize Logger
            logger = NLog.LogManager.GetCurrentClassLogger();

            settings = App.settings;
            tsManagerStarted = false;
            scheduledTaskRunner = new ScheduledTaskRunner();
            legalNoticeSetter = new LegalNoticeSetter();
            settingsValidator = new SettingsValidator();
            cleanup = new Cleanup();
            errorHandler = new ErrorHandler();

            logSharePath = settings.GetSettingsItemValue("Other", "LogSharePath");
            logFile = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("Other", "LogFilePath"), true).ValidatedPath;
        }

        public void RunTaskSequence(string taskSequenceName)
        {
            smsClient = ConnectSMSClient(); // establish an SMSClient connection
            processName = "TSManager.exe";

            if (smsClient != null)
            {
                // Find and run the target Task Sequence
                StartTaskSequence(taskSequenceName);

                // Invoke process watcher in its own thread to monitor for the start of the Task Sequence
                var backgroundThread100 = new Thread(() => WatchForProcess(processName));
                backgroundThread100.Start();
            }
            else
            {
                // Exit the application if an SMSClient connection is unsuccessful
                logger.Error("SMSClient is null.");
                FailGracefully("SMSClientFailure");
            }
        }

        private SMSClient ConnectSMSClient()
        {
            string computerName = Environment.MachineName;

            SMSClient smsClient = null;
            logger.Info("Trying to establish SMSClient connection.");
            try
            {
                smsClient = new SMSClient(computerName);
                logger.Info("Successfully established SMSClient connection.");
            }
            catch (Exception e)
            {
                // Couldn't connect to SMSClient
                logger.Error($"Unable to establish SMSClient connection: {e}");
                FailGracefully("SMSClientConnectionFailure");
            }

            return smsClient;
        }

        private ManagementBaseObject GetAdvertisement(string advertisementName)
        {
            ManagementBaseObject targetAdvertisement = null;

            // Request machine policy
            logger.Info("Requesting Machine Policy.");
            try
            {
                smsClient.RequestMachinePolicy();

                // Enumerate package advertisements currently present on the machine
                logger.Info("Enumerating list of Advertisements present on the machine.");
                var advertisementQuery = from ManagementBaseObject adv in smsClient.SoftwareDistribution.Advertisements
                                         where adv["PKG_Name"].ToString() == advertisementName
                                         select adv;

                logger.Info($"Advertisement Count: {advertisementQuery.Count()}");
                if (advertisementQuery.Count() == 1)
                {
                    foreach (var advertisement in advertisementQuery)
                    {
                        logger.Info($"Found Advertisement '{advertisement}'.");
                        targetAdvertisement = advertisement;
                        break;
                    }
                }
                else if (advertisementQuery.Count() > 1)
                {
                    logger.Error($"Found multiple Advertisements that match '{advertisementName}'");

                }
                else
                {
                    logger.Error($"Unable to find Advertisement that matches '{advertisementName}'");
                }
            } catch (Exception e) {
                logger.Error($"Error Requesting Machine Policy: {e}");
            }            

            return targetAdvertisement;
        }

        private void StartTaskSequence(string taskSequenceName)
        {
            int getAdvertisementAttempt = 0;
            int getAdvertisementAttemptLimit = 5;
            
            // Find the task sequence advertisement to trigger
            ManagementBaseObject advertisement = null;
            advertisement = GetAdvertisement(taskSequenceName);
            while ((advertisement == null) && (getAdvertisementAttempt < getAdvertisementAttemptLimit))
            {
                // Did not retrieve the specified advertisement. The policy may still be updating, trying again.
                Thread.Sleep(5000);
                logger.Warn($"The specified Advertisement '{taskSequenceName}' was not found. Will try {getAdvertisementAttemptLimit - getAdvertisementAttempt} more times to retrieve it.");
                advertisement = GetAdvertisement(taskSequenceName);
                getAdvertisementAttempt++;                
            }

            if (advertisement != null)
            {
                // Trigger Advertisement Rerun of the target Task Sequence
                try
                {
                    logger.Info($"Trying to run Task Sequence with AdvertisementID '{advertisement["ADV_AdvertisementID"].ToString()}' and PackageID '{advertisement["PKG_PackageID"].ToString()}'");
                    smsClient.SoftwareDistribution.RerunAdv(advertisement["ADV_AdvertisementID"].ToString(), advertisement["PKG_PackageID"].ToString(), "*");

                    logger.Info("Successfully ran Task Sequence Advertisement.");
                }
                catch (Exception e)
                {
                    logger.Error($"Failed to run Task Sequence Advertisement: {e}");
                    FailGracefully("TSRunFailure");
                }
            } else
            {
                logger.Error($"Failed to retrieve Advertisement '{taskSequenceName}' after {getAdvertisementAttemptLimit} tries. Exiting.");
                FailGracefully("AdvRetrieveFailure");
            }
        }

        public void FailGracefully(string failureReason)
        {
            // Remove Run Task Sequence Scheduled Task
            string runTsScheduledTaskPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "RunTSXmlPath"), true).ValidatedPath;
            scheduledTaskRunner.UnregisterScheduledTask(runTsScheduledTaskPath);

            // Register the Post Failure Cleanup Scheduled Task
            string cleanupScheduledTaskPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "CleanupXmlPath"), true).ValidatedPath;
            scheduledTaskRunner.RegisterScheduledTask(null, cleanupScheduledTaskPath);

            // Set Failure Message
            string legalNoticeCaptionOnStartFailure = settings.GetSettingsItemValue("PostGUI", "LegalNoticeCaptionOnStartFailure");
            string legalNoticeTextOnStartFailure = settings.GetSettingsItemValue("PostGUI", "LegalNoticeTextOnStartFailure");
            legalNoticeSetter.SetLegalNoticeRegKeys(legalNoticeCaptionOnStartFailure, legalNoticeTextOnStartFailure);

            // Remove AttemptNumber registry key
            this.ClearAttemptRegKey();

            // Remove UPGBackground
            cleanup.RemoveUPGBackground();

            // Copy logs to the log share
            this.errorHandler.CopyLogs(failureReason);
            // CopyLogs(failureReason);

            // Restart machine
            App.RestartComputer();

            // Exit
            Environment.Exit(Environment.ExitCode);
        }

        private void WatchForProcess(string processName)
        {
            logger.Info($"Creating Process Watcher to watch for process '{processName}'.");
            // ManagementEventWatcher startWatch;            
            try
            {
                // Define the process watcher query
                WqlEventQuery query = new WqlEventQuery($"SELECT * FROM Win32_ProcessStartTrace WHERE ProcessName = '{processName}'");

                // Create the process watcher
                this.startWatch = new ManagementEventWatcher(query);
                this.startWatch.EventArrived += new EventArrivedEventHandler(StartWatch_EventArrived);
                this.startWatch.Start();

                logger.Info("Successfully registered Process Watcher.");

                // Begin waiting for process to start
                int attemptLimit = 4;
                int currentAttempt = GetAttemptRegKey();
                if (currentAttempt < 0)
                {
                    currentAttempt = 1;
                }
                bool loopResult = false;
                while(!loopResult && (currentAttempt <= attemptLimit))
                {
                    int waitMs = 5000;
                    int timeoutMinutes;
                    logger.Info($"Starting wait loop for process to start. Attempt {currentAttempt} of {attemptLimit}");
                    switch (currentAttempt)
                    {
                        case 1:                             
                            timeoutMinutes = 5; 
                            loopResult = this.StartWaitLoop(waitMs, timeoutMinutes);
                            if (!loopResult)
                            {
                                logger.Warn($"The process did not start after the specified timeout period of {timeoutMinutes} minutes");
                                string taskSequenceName = App.taskSequenceName;
                                logger.Warn($"Trying to rerun Task Sequence '{taskSequenceName}'");
                                this.StartTaskSequence(taskSequenceName);
                            }
                            break;
                        case 2:
                            timeoutMinutes = 10; 
                            loopResult = this.StartWaitLoop(waitMs, timeoutMinutes);
                            if (!loopResult)
                            {
                                logger.Warn($"The process did not start after the specified timeout period of {timeoutMinutes} minutes");
                                string taskSequenceName = App.taskSequenceName;
                                logger.Warn($"Trying to rerun Task Sequence '{taskSequenceName}' again...");
                                this.StartTaskSequence(taskSequenceName);
                            }
                            break;
                        case 3:
                            timeoutMinutes = 5;
                            loopResult = this.StartWaitLoop(waitMs, timeoutMinutes);
                            if (!loopResult)
                            {
                                logger.Warn($"The process did not start after the specified timeout period of {timeoutMinutes} minutes");
                                logger.Warn($"Setting AttemptNumber reg key and restarting computer to try one last time");
                                this.SetAttemptRegKey(4); // Set the AttemptNumber reg key so that we can pull this number from the registry upon reboot

                                App.RestartComputer();
                            }
                            break;
                        case 4:
                            timeoutMinutes = 10;
                            loopResult = this.StartWaitLoop(waitMs, timeoutMinutes);
                            if (!loopResult)
                            {
                                logger.Warn($"The process did not start after the specified timeout period of {timeoutMinutes} minutes");
                                logger.Warn($"This was the final attempt");
                            }
                            break;
                    }

                    currentAttempt++;
                }

                if (loopResult)
                {
                    // Once the process is found, kill the process watcher and exit the application
                    logger.Info($"Successfully started '{processName}'. Stopping Process Watcher and exiting.");
                    startWatch.Stop();
                    startWatch.Dispose();

                    // Remove AttemptNumber registry key
                    try
                    {
                        this.ClearAttemptRegKey();
                    } catch { }
                   

                    // Remove Run Task Sequence Scheduled Task
                    string runTsScheduledTaskPath = settingsValidator.ValidateSettingsItemPath(settings.GetSettingsItem("ScheduledTask", "RunTSXmlPath"), true).ValidatedPath;
                    scheduledTaskRunner.UnregisterScheduledTask(runTsScheduledTaskPath);

                    // Exit
                    Environment.Exit(Environment.ExitCode);
                }
                else
                {
                    // Process was not started after specified amount of attempts.
                    logger.Error($"The process did not start after {attemptLimit} attempts. Giving up.");
                    FailGracefully("ProcWatchTimeout");
                }

            } catch (Exception e)
            {
                logger.Error($"Unable to create Process Watcher: {e}");
                FailGracefully("ProcWatchCreateFailure");
            }
        }

        private bool StartWaitLoop(int waitMs, int timeoutMinutes)
        {
            bool result = false;
            // Loop until the target process is detected, waiting a specified amount of time between checks
            DateTime timeStarted = DateTime.Now;
            DateTime timeNow = DateTime.Now;
            TimeSpan timeoutTimeSpan = new TimeSpan(0, timeoutMinutes, 0);
            logger.Info($"Waiting for the process to start for maximum of {timeoutMinutes} minutes ({timeoutTimeSpan.TotalSeconds} seconds) before giving up.");

            while ((!tsManagerStarted) && ((timeNow - timeStarted).TotalSeconds <= timeoutTimeSpan.TotalSeconds))
            {
                timeNow = DateTime.Now;
                TimeSpan timeElapsed = timeNow - timeStarted;
                int secondsElapsed = (int)timeElapsed.TotalSeconds;
                logger.Info($"{processName} hasn't started yet. Sleeping for {waitMs} ms and checking again.");
                Thread.Sleep(waitMs);
            }

            if (tsManagerStarted)
            {
                // Process detected
                result = true;
                return result;
            }
            else
            {
                // Process not detected
                result = false;
                return result;
            }
        }


        private void SetAttemptRegKey(int attemptNumber)
        {
            logger.Info($"Start setting AttemptNumber registry key.");

            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            }
            else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                string regKeyPath = @"SOFTWARE\\Windows Feature Update UI";
                
                // check if key exists
                if (regLocalMachine.OpenSubKey(regKeyPath, false) == null)
                {
                    // key doesn't exist. Create it first
                    logger.Warn($"Registry key '{regKeyPath}' doesn't exist yet... Creating it...");
                    regLocalMachine.CreateSubKey(regKeyPath);
                }

                using (RegistryKey regKey = regLocalMachine.OpenSubKey(regKeyPath, true))
                {
                    logger.Warn($"Setting '{regKey.Name} | AttemptNumber' with value '{attemptNumber}'");
                    regKey.SetValue("AttemptNumber", attemptNumber, RegistryValueKind.DWord);
                }
            }
        }

        private int GetAttemptRegKey()
        {
            logger.Info($"Start getting AttemptNumber registry key.");
            int result = -1;

            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            }
            else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                string regKeyPath = @"SOFTWARE\\Windows Feature Update UI";

                // check if key exists
                if (regLocalMachine.OpenSubKey(regKeyPath, false) == null)
                {
                    // key doesn't exist
                    logger.Warn($"Registry key '{regKeyPath}' doesn't exist. Returning {result}");
                }
                else
                {
                    // key exists
                    using (RegistryKey regKey = regLocalMachine.OpenSubKey(regKeyPath, true))
                    {
                        logger.Info($"Registry key '{regKeyPath}' exists. Checking if AttemptNumber has a value");
                        var value = regKey.GetValue("AttemptNumber");
                        if (value != null)
                        {
                            // AttemptNumber exists
                            logger.Info($"AttemptNumber has a value of {value}. Returning {value}");
                            result = (int)value;
                        } else
                        {
                            // AttemptNumber doesn't exist
                            logger.Warn($"AttemptNumber doesn't exist or has no value. Returning {result}");
                        }
                    }
                }
            }

            return result;
        }

        private void ClearAttemptRegKey()
        {
            logger.Info($"Start clearing AttemptNumber registry key.");

            // Set the registry view depending on the bitness of the process
            RegistryView regView;
            if ((Environment.Is64BitOperatingSystem) && (IntPtr.Size == 4))
            {
                regView = RegistryView.Registry64;
            }
            else
            {
                regView = RegistryView.Default;
            }

            string computerName = Environment.MachineName;

            // Connect to the registry provider
            using (RegistryKey regLocalMachine = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName, regView))
            {
                string regKeyPath = @"SOFTWARE\\Windows Feature Update UI";

                // check if key exists
                if (regLocalMachine.OpenSubKey(regKeyPath, false) == null)
                {
                    // key doesn't exist
                    logger.Info($"Registry key '{regKeyPath}' doesn't exist. Nothing to clear");
                }
                else
                {
                    // key exists
                    using (RegistryKey regKey = regLocalMachine.OpenSubKey(regKeyPath, true))
                    {
                        logger.Info($"Registry key '{regKeyPath}' exists. Checking if AttemptNumber exists");
                        var value = regKey.GetValue("AttemptNumber");
                        if (value != null)
                        {
                            // AttemptNumber exists. Clear it
                            logger.Info($"AttemptNumber exists. Clearing it");
                            regKey.DeleteValue("AttemptNumber");
                        }
                        else
                        {
                            // AttemptNumber doesn't exist
                            logger.Warn($"AttemptNumber doesn't exist or has no value. Nothing to clear");
                        }
                    }
                }
            }
        }

        /*
        private void StopWatch_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Warn($"Process Watcher detected stop of process '{processName}'.");
        }
        */
        private void StartWatch_EventArrived(object sender, EventArrivedEventArgs e)
        {
            logger.Warn($"Process Watcher detected start of process '{processName}'.");
            tsManagerStarted = true;
        }

        /*
        public void CopyLogs(string description)
        {
            string remoteLogFileName = $"{Environment.MachineName}_{description}_{DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss")}.log";
            string remoteLogFilePath = $"{logSharePath}\\{remoteLogFileName}";
            
            try
            {
                System.IO.File.OpenWrite(remoteLogFilePath).Close();
                logger.Info($"Successfully wrote to '{remoteLogFilePath}'");
                System.IO.File.Delete(remoteLogFilePath);
                System.IO.File.Copy(logFile, remoteLogFilePath);
            } catch (Exception e)
            {                
                logger.Error($"Error writing/copying to '{remoteLogFilePath}'");
                logger.Error(e);
            }
        }
        */
    }    
}
